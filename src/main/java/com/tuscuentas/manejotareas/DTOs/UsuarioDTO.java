package com.tuscuentas.manejotareas.DTOs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tuscuentas.manejotareas.common.util.Generales;
import java.io.Serializable;

/**
 *
 * @author DIEGO
 */
public class UsuarioDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String tipoDocumento = Generales.EMPTYSTRING;
    String documento = Generales.EMPTYSTRING;
    String idUsuarioLogueado = Generales.EMPTYSTRING;
    String idTipoUsuarioLogueado = Generales.EMPTYSTRING;
    String idTipoUsuario = Generales.EMPTYSTRING;
    String primerNombre = Generales.EMPTYSTRING;
    String otrosNombres = Generales.EMPTYSTRING;
    String primerApellido = Generales.EMPTYSTRING;
    String segundoApellido = Generales.EMPTYSTRING;
    String celular = Generales.EMPTYSTRING;
    String correo = Generales.EMPTYSTRING;
    String imagen = Generales.EMPTYSTRING;
    String usuario = Generales.EMPTYSTRING;
    String clave = Generales.EMPTYSTRING;
    String estado = Generales.EMPTYSTRING;
    String municipio = Generales.EMPTYSTRING;
    String codigo = Generales.EMPTYSTRING;
    String fechaNacimiento = Generales.EMPTYSTRING;
    String imagenPerfil = Generales.EMPTYSTRING;
    String genero = Generales.EMPTYSTRING;
    String lugarNacimiento = Generales.EMPTYSTRING;
    String departamentoNacimiento = Generales.EMPTYSTRING;
    String rol = Generales.EMPTYSTRING;
    String idUsuario = Generales.EMPTYSTRING;
    String idTipoDocumento = Generales.EMPTYSTRING;
    String tipoUsuario = Generales.EMPTYSTRING;
    String idMunicipio = Generales.EMPTYSTRING;
    String idDepartamento = Generales.EMPTYSTRING;
    String departamento = Generales.EMPTYSTRING;
    String direccion = Generales.EMPTYSTRING;
    String telefono = Generales.EMPTYSTRING;
    String idGenero = Generales.EMPTYSTRING;
    String semana = Generales.EMPTYSTRING;
    String idProyecto = Generales.EMPTYSTRING;
    String idEmpresa = Generales.EMPTYSTRING;
    String idCargoEmpresa = Generales.EMPTYSTRING;
    String cargoEmpresa = Generales.EMPTYSTRING;
    String contadorPrincipal = Generales.ESTADO_INACTIVO;
    String idContadorJuridico = Generales.EMPTYSTRING;
    String numTarjeta = Generales.EMPTYSTRING;
    String idSupervisor = Generales.EMPTYSTRING;
    String registradoPor = Generales.EMPTYSTRING;
    String fechaRegistro = Generales.EMPTYSTRING;
    String eficacia = Generales.EMPTYSTRING;
    String tareasAbiertas = Generales.EMPTYSTRING;
    String tareasAtrasadas = Generales.EMPTYSTRING;
    String tareasPendientes = Generales.EMPTYSTRING;
    String token = Generales.EMPTYSTRING;
    String tokenApp = Generales.EMPTYSTRING;
    String esNuevo = Generales.EMPTYSTRING;
    String numeroContrato = Generales.EMPTYSTRING;
    String empresaUsuario = Generales.EMPTYSTRING;
    String nit = Generales.EMPTYSTRING;
    String dv = Generales.EMPTYSTRING;
    String claveLearning = Generales.EMPTYSTRING;
    String estadoContador = Generales.EMPTYSTRING;
    String nombreVendedor = Generales.EMPTYSTRING;
    String interventor = Generales.EMPTYSTRING;
    String responsableAlertas = Generales.EMPTYSTRING;

    /**
     *
     * @return
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     *
     * @param departamento
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     *
     * @return
     */
    public String getEficacia() {
        return eficacia;
    }

    /**
     *
     * @param eficacia
     */
    public void setEficacia(String eficacia) {
        this.eficacia = eficacia;
    }

    /**
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     *
     * @param tipoDocumento
     */
    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    /**
     *
     * @return
     */
    public String getDocumento() {
        return documento;
    }

    /**
     *
     * @param documento
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     *
     * @return
     */
    public String getIdUsuarioLogueado() {
        return idUsuarioLogueado;
    }

    /**
     *
     * @param idUsuarioLogueado
     */
    public void setIdUsuarioLogueado(String idUsuarioLogueado) {
        this.idUsuarioLogueado = idUsuarioLogueado;
    }

    /**
     *
     * @return
     */
    public String getIdTipoUsuarioLogueado() {
        return idTipoUsuarioLogueado;
    }

    /**
     *
     * @param idTipoUsuarioLogueado
     */
    public void setIdTipoUsuarioLogueado(String idTipoUsuarioLogueado) {
        this.idTipoUsuarioLogueado = idTipoUsuarioLogueado;
    }

    /**
     *
     * @return
     */
    public String getIdTipoUsuario() {
        return idTipoUsuario;
    }

    /**
     *
     * @param idTipoUsuario
     */
    public void setIdTipoUsuario(String idTipoUsuario) {
        this.idTipoUsuario = idTipoUsuario;
    }

    /**
     *
     * @return
     */
    public String getPrimerNombre() {
        return primerNombre;
    }

    /**
     *
     * @param primerNombre
     */
    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    /**
     *
     * @return
     */
    public String getOtrosNombres() {
        return otrosNombres;
    }

    /**
     *
     * @param otrosNombres
     */
    public void setOtrosNombres(String otrosNombres) {
        this.otrosNombres = otrosNombres;
    }

    /**
     *
     * @return
     */
    public String getPrimerApellido() {
        return primerApellido;
    }

    /**
     *
     * @param primerApellido
     */
    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    /**
     *
     * @return
     */
    public String getSegundoApellido() {
        return segundoApellido;
    }

    /**
     *
     * @param segundoApellido
     */
    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    /**
     *
     * @return
     */
    public String getCelular() {
        return celular;
    }

    /**
     *
     * @param celular
     */
    public void setCelular(String celular) {
        this.celular = celular;
    }

    /**
     *
     * @return
     */
    public String getCorreo() {
        return correo;
    }

    /**
     *
     * @param correo
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     *
     * @return
     */
    public String getImagen() {
        return imagen;
    }

    /**
     *
     * @param imagen
     */
    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    /**
     *
     * @return
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     *
     * @param usuario
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     *
     * @return
     */
    public String getClave() {
        return clave;
    }

    /**
     *
     * @param clave
     */
    public void setClave(String clave) {
        this.clave = clave;
    }

    /**
     *
     * @return
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     *
     * @return
     */
    public String getMunicipio() {
        return municipio;
    }

    /**
     *
     * @param municipio
     */
    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    /**
     *
     * @return
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     *
     * @param codigo
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     *
     * @return
     */
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     *
     * @param fechaNacimiento
     */
    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     *
     * @return
     */
    public String getImagenPerfil() {
        return imagenPerfil;
    }

    /**
     *
     * @param imagenPerfil
     */
    public void setImagenPerfil(String imagenPerfil) {
        this.imagenPerfil = imagenPerfil;
    }

    /**
     *
     * @return
     */
    public String getGenero() {
        return genero;
    }

    /**
     *
     * @param genero
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }

    /**
     *
     * @return
     */
    public String getLugarNacimiento() {
        return lugarNacimiento;
    }

    /**
     *
     * @param lugarNacimiento
     */
    public void setLugarNacimiento(String lugarNacimiento) {
        this.lugarNacimiento = lugarNacimiento;
    }

    /**
     *
     * @return
     */
    public String getDepartamentoNacimiento() {
        return departamentoNacimiento;
    }

    /**
     *
     * @param departamentoNacimiento
     */
    public void setDepartamentoNacimiento(String departamentoNacimiento) {
        this.departamentoNacimiento = departamentoNacimiento;
    }

    /**
     *
     * @return
     */
    public String getRol() {
        return rol;
    }

    /**
     *
     * @param rol
     */
    public void setRol(String rol) {
        this.rol = rol;
    }

    /**
     *
     * @return
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     *
     * @return
     */
    public String getIdTipoDocumento() {
        return idTipoDocumento;
    }

    /**
     *
     * @param idTipoDocumento
     */
    public void setIdTipoDocumento(String idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    /**
     *
     * @return
     */
    public String getTipoUsuario() {
        return tipoUsuario;
    }

    /**
     *
     * @param tipoUsuario
     */
    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    /**
     *
     * @return
     */
    public String getIdMunicipio() {
        return idMunicipio;
    }

    /**
     *
     * @param idMunicipio
     */
    public void setIdMunicipio(String idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    /**
     *
     * @return
     */
    public String getIdDepartamento() {
        return idDepartamento;
    }

    /**
     *
     * @param idDepartamento
     */
    public void setIdDepartamento(String idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    /**
     *
     * @return
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     *
     * @param direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     *
     * @return
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     *
     * @param telefono
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     *
     * @return
     */
    public String getIdGenero() {
        return idGenero;
    }

    /**
     *
     * @param idGenero
     */
    public void setIdGenero(String idGenero) {
        this.idGenero = idGenero;
    }

    /**
     *
     * @return
     */
    public String getSemana() {
        return semana;
    }

    /**
     *
     * @param semana
     */
    public void setSemana(String semana) {
        this.semana = semana;
    }

    /**
     *
     * @return
     */
    public String getIdProyecto() {
        return idProyecto;
    }

    /**
     *
     * @param idProyecto
     */
    public void setIdProyecto(String idProyecto) {
        this.idProyecto = idProyecto;
    }

    /**
     *
     * @return
     */
    public String getIdEmpresa() {
        return idEmpresa;
    }

    /**
     *
     * @param idEmpresa
     */
    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    /**
     *
     * @return
     */
    public String getIdCargoEmpresa() {
        return idCargoEmpresa;
    }

    /**
     *
     * @param idCargoEmpresa
     */
    public void setIdCargoEmpresa(String idCargoEmpresa) {
        this.idCargoEmpresa = idCargoEmpresa;
    }

    /**
     *
     * @return
     */
    public String getCargoEmpresa() {
        return cargoEmpresa;
    }

    /**
     *
     * @param cargoEmpresa
     */
    public void setCargoEmpresa(String cargoEmpresa) {
        this.cargoEmpresa = cargoEmpresa;
    }

    /**
     *
     * @return
     */
    public String getContadorPrincipal() {
        return contadorPrincipal;
    }

    /**
     *
     * @param contadorPrincipal
     */
    public void setContadorPrincipal(String contadorPrincipal) {
        this.contadorPrincipal = contadorPrincipal;
    }

    /**
     *
     * @return
     */
    public String getIdContadorJuridico() {
        return idContadorJuridico;
    }

    /**
     *
     * @param idContadorJuridico
     */
    public void setIdContadorJuridico(String idContadorJuridico) {
        this.idContadorJuridico = idContadorJuridico;
    }

    /**
     *
     * @return
     */
    public String getNumTarjeta() {
        return numTarjeta;
    }

    /**
     *
     * @param numTarjeta
     */
    public void setNumTarjeta(String numTarjeta) {
        this.numTarjeta = numTarjeta;
    }

    /**
     *
     * @return
     */
    public String getIdSupervisor() {
        return idSupervisor;
    }

    /**
     *
     * @param idSupervisor
     */
    public void setIdSupervisor(String idSupervisor) {
        this.idSupervisor = idSupervisor;
    }

    /**
     *
     * @return
     */
    public String getRegistradoPor() {
        return registradoPor;
    }

    /**
     *
     * @param registradoPor
     */
    public void setRegistradoPor(String registradoPor) {
        this.registradoPor = registradoPor;
    }

    /**
     *
     * @return
     */
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    /**
     *
     * @param fechaRegistro
     */
    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    /**
     *
     * @return
     */
    public String getTareasAbiertas() {
        return tareasAbiertas;
    }

    /**
     *
     * @param tareasAbiertas
     */
    public void setTareasAbiertas(String tareasAbiertas) {
        this.tareasAbiertas = tareasAbiertas;
    }

    /**
     *
     * @return
     */
    public String getTareasAtrasadas() {
        return tareasAtrasadas;
    }

    /**
     *
     * @param tareasAtrasadas
     */
    public void setTareasAtrasadas(String tareasAtrasadas) {
        this.tareasAtrasadas = tareasAtrasadas;
    }

    /**
     *
     * @return
     */
    public String getTareasPendientes() {
        return tareasPendientes;
    }

    /**
     *
     * @param tareasPendientes
     */
    public void setTareasPendientes(String tareasPendientes) {
        this.tareasPendientes = tareasPendientes;
    }

    /**
     *
     * @return
     */
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     *
     * @return
     */
    public String getTokenApp() {
        return tokenApp;
    }

    /**
     *
     * @param tokenApp
     */
    public void setTokenApp(String tokenApp) {
        this.tokenApp = tokenApp;
    }

    /**
     *
     * @return
     */
    public String getEsNuevo() {
        return esNuevo;
    }

    /**
     *
     * @param esNuevo
     */
    public void setEsNuevo(String esNuevo) {
        this.esNuevo = esNuevo;
    }

    /**
     *
     * @return
     */
    public String getNumeroContrato() {
        return numeroContrato;
    }

    /**
     *
     * @param numeroContrato
     */
    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    /**
     *
     * @return
     */
    public String getEmpresaUsuario() {
        return empresaUsuario;
    }

    /**
     *
     * @param empresaUsuario
     */
    public void setEmpresaUsuario(String empresaUsuario) {
        this.empresaUsuario = empresaUsuario;
    }

    /**
     *
     * @return
     */
    public String getNit() {
        return nit;
    }

    /**
     *
     * @param nit
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     *
     * @return
     */
    public String getDv() {
        return dv;
    }

    /**
     *
     * @param dv
     */
    public void setDv(String dv) {
        this.dv = dv;
    }

    /**
     *
     * @return
     */
    public String getClaveLearning() {
        return claveLearning;
    }

    /**
     *
     * @param claveLearning
     */
    public void setClaveLearning(String claveLearning) {
        this.claveLearning = claveLearning;
    }

    /**
     *
     * @return
     */
    public String getEstadoContador() {
        return estadoContador;
    }

    /**
     *
     * @param estadoContador
     */
    public void setEstadoContador(String estadoContador) {
        this.estadoContador = estadoContador;
    }

    /**
     *
     * @return
     */
    public String getNombreVendedor() {
        return nombreVendedor;
    }

    /**
     *
     * @param nombreVendedor
     */
    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    public String getInterventor() {
        return interventor;
    }

    public void setInterventor(String interventor) {
        this.interventor = interventor;
    }

    public String getResponsableAlertas() {
        return responsableAlertas;
    }

    public void setResponsableAlertas(String responsableAlertas) {
        this.responsableAlertas = responsableAlertas;
    }

    /**
     *
     * @return
     */
    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

}
