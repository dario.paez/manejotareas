/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuscuentas.manejotareas.proceso;

import com.tuscuentas.manejotareas.DTOs.ActividadPreDTO;
import com.tuscuentas.manejotareas.DTOs.ActividadPredeterminadaDTO;
import com.tuscuentas.manejotareas.DTOs.CheckListDTO;
import com.tuscuentas.manejotareas.DTOs.EjecucionProximaDTO;
import com.tuscuentas.manejotareas.DTOs.EjecucionTareaDTO;
import com.tuscuentas.manejotareas.DTOs.EmpresaDTO;
import com.tuscuentas.manejotareas.DTOs.LogActividadesDTO;
import com.tuscuentas.manejotareas.DTOs.RespuestaVistaDTO;
import com.tuscuentas.manejotareas.DTOs.TareaPredeterminadaDTO;
import com.tuscuentas.manejotareas.DTOs.UsuarioDTO;
import com.tuscuentas.manejotareas.common.util.Constantes;
import com.tuscuentas.manejotareas.common.util.Generales;
import com.tuscuentas.manejotareas.consultas.ConsultasMySQL;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author dario
 */
public class ProcesoTareasDiarias {

    static ConsultasMySQL consultas = null;
    static ArrayList<String> listadoDias = null;

    public static void main(String[] args) throws SQLException {
        // TODO code application logic here

        consultas = new ConsultasMySQL();
        listadoDias = consultas.listarDiasFestivos();
        ArrayList<UsuarioDTO> listUsuarios = new ArrayList<>();
        listUsuarios = consultas.listarUsuarios();
        ArrayList<EjecucionTareaDTO> listado = null;

        System.out.println(consultas.actualizarEstadoAtrasada().getMensaje());
        System.out.println(consultas.actualizarEstadoAbierto().getMensaje());

        if (listUsuarios != null && !listUsuarios.isEmpty()) {
            for (int i = 0; i < listUsuarios.size(); i++) {

                /**
                 * Se listan las actividades atrasadas por usuasio
                 */
                listado = consultas.listarActividadesAtrasadasPorUsuarioServletTareaProgramada(listUsuarios.get(i).getId(), "0");

                if (listado != null && !listado.isEmpty()) {
                    /**
                     * se manda el listado de ejecuciones atrasadas para
                     * realizar el cargue de las proximas ejecuciones
                     */
                    realizarCargueProximaTareaAtrasadas(listado);

                    /**
                     * Se registra log de actividades atrasadas
                     */
                    actualizarLogTareasAtrasadas(listado, listUsuarios.get(i).getId(), listUsuarios.get(i).getIdContadorJuridico());
                } else {
                    System.out.println(" |   El listado de actividades por usuario es vacio para el usuario : " + listUsuarios.get(i).getId());
                }
            }

        } else {
            System.out.println(" |   El listado de usuarios es null");
        }
    }

    public static void realizarCargueProximaTareaAtrasadas(ArrayList<EjecucionTareaDTO> listado) {

//        System.out.println("  |  realizarCargueProximaTareaAtrasadas");
        EjecucionTareaDTO datosEjecucion = null;
        RespuestaVistaDTO respuestaCargue = null;
        EjecucionProximaDTO validar = null;

        try {

            if (listado != null && !listado.isEmpty()) {
                for (int i = 0; i < listado.size(); i++) {

                    int codigoEjecucion = (Integer.parseInt(listado.get(i).getCodEjcucion()) + 1);

                    validar = consultas.validarEjecucionProxima(Integer.toString(codigoEjecucion), listado.get(i).getIdTareaEmpresa());

                    if (validar == null) {

                        datosEjecucion = consultas.consultarTareaEmpresaPorIdEjecucion(listado.get(i).getId());

                        if (datosEjecucion != null) {
                            validar = consultas.listarActividadPorEjecutar(datosEjecucion.getIdTareaEmpresa());

                            if (validar != null) {
                                respuestaCargue = realizarCargueProximaTarea(listado.get(i).getIdTareaEmpresa(), listado.get(i).getIdEmpresa(), validar.getFechaInicialProxima(), true);
                                if (respuestaCargue == null || !respuestaCargue.isRegistro()) {
                                    System.out.println("  | No se pudo realizar el cargue ERROR");
                                }
                            }
                        } else {
                            System.out.println("  |No se pudo realizar el cargue ERROR");
                        }
                    } else {
//                        System.out.println("  |Ya existe esa tarea empresa con este codigo de ejecucion" + listado.get(i).getCodEjcucion());
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // return listado;
    }

    public static RespuestaVistaDTO realizarCargueProximaTarea(String idTareaEmpresa, String idEmpresa, String fechaInicial, boolean ejecutar) {
//        System.out.println("  | realizarCargueProximaTarea ");
        RespuestaVistaDTO respuesta = null;
        ArrayList<TareaPredeterminadaDTO> listadoTareaPre = null;
        ArrayList<ActividadPredeterminadaDTO> actividades = null;
        ActividadPredeterminadaDTO actividad = null;
        EmpresaDTO empresa = null;
        EjecucionTareaDTO ejecucionRegistrar = null;
        EjecucionTareaDTO ejecucion = null;
        ArrayList<EjecucionTareaDTO> listadoEjecucion = null;
        boolean bandera = false;
        Map<String, ArrayList<CheckListDTO>> datosChecklist = new HashMap<>();
        ArrayList<CheckListDTO> lista = null;
        String ultimaEjecucion = Generales.EMPTYSTRING;
        String taemId = Generales.EMPTYSTRING, acprId = Generales.EMPTYSTRING, variable = Generales.EMPTYSTRING;
        TareaPredeterminadaDTO tareaPro = null;
        EjecucionProximaDTO datosEjecucionProxima = null;

        try {
            listadoTareaPre = consultas.listarTareaPredeterminadaPorIdTareaEmpresa(idTareaEmpresa);

            if (listadoTareaPre != null && !listadoTareaPre.isEmpty()) {

                empresa = consultas.obtenerInfoEmpresaPorId(idEmpresa);
                if (empresa == null) {
                    throw new Exception("Error : error ejecutando el cargue para la siguiente tarea empresa: " + idTareaEmpresa);
                }

                actividades = new ArrayList<ActividadPredeterminadaDTO>();
                //<editor-fold defaultstate="collapsed" desc="Bloque para agregar los usuarios a las ejecuions">

                if (listadoTareaPre.get(0).getEstadoPasoEjecucion() != null && listadoTareaPre.get(0).getEstadoPasoEjecucion().equals("1")) {
                    actividad = new ActividadPredeterminadaDTO();
                    UsuarioDTO usuario = consultas.getUserByCargoEmpresa(listadoTareaPre.get(0).getCargoEjecucion(), idEmpresa);
                    if (usuario != null && !usuario.getId().equals("")) {
                        actividad.setIdUsuarioPorCargo(usuario.getId());
                    } else {
                        UsuarioDTO usuarioContador = consultas.getUserByCargoEmpresaAndContador(listadoTareaPre.get(0).getCargoEjecucion(), empresa);
                        if (usuarioContador != null) {
                            actividad.setIdUsuarioPorCargo(usuarioContador.getId());
                        } else {
                            actividad.setIdUsuarioPorCargo(empresa.getIdContador());
                        }
                    }
                    actividad.setIdTarea(listadoTareaPre.get(0).getId());
                    actividad.setActividad(listadoTareaPre.get(0).getIdPasoEjecucion());
                    actividad.setDiasDuracion(listadoTareaPre.get(0).getDiasEjecucion());
                    actividad.setEstado(listadoTareaPre.get(0).getEstadoPasoEjecucion());
                    actividad.setDiasProrroga(listadoTareaPre.get(0).getProrrogaEjecucion());
                    actividad.setCargoActividad(listadoTareaPre.get(0).getCargoEjecucion());
                    actividad.setDescripcion("EJECUCIÓN");
                    actividad.setOrdenActividad("1");

                    actividades.add(actividad);
                }

                if (listadoTareaPre.get(0).getEstadoPasoRevision() != null && listadoTareaPre.get(0).getEstadoPasoRevision().equals("1")) {
                    actividad = new ActividadPredeterminadaDTO();
                    UsuarioDTO usuario = consultas.getUserByCargoEmpresa(listadoTareaPre.get(0).getCargoRevision(), idEmpresa);
                    if (usuario != null && !usuario.getId().equals("")) {
                        actividad.setIdUsuarioPorCargo(usuario.getId());
                    } else {
                        UsuarioDTO usuarioContador = consultas.getUserByCargoEmpresaAndContador(listadoTareaPre.get(0).getCargoRevision(), empresa);
                        if (usuarioContador != null) {
                            actividad.setIdUsuarioPorCargo(usuarioContador.getId());
                        } else {
                            actividad.setIdUsuarioPorCargo(empresa.getIdContador());
                        }
                    }
                    actividad.setIdTarea(listadoTareaPre.get(0).getId());
                    actividad.setActividad(listadoTareaPre.get(0).getIdPasoRevision());
                    actividad.setDiasDuracion(listadoTareaPre.get(0).getDiasRevision());
                    actividad.setEstado(listadoTareaPre.get(0).getEstadoPasoRevision());
                    actividad.setDiasProrroga(listadoTareaPre.get(0).getProrrogaRevision());
                    actividad.setCargoActividad(listadoTareaPre.get(0).getCargoRevision());
                    actividad.setDescripcion("REVISIÓN");
                    actividad.setOrdenActividad("2");

                    actividades.add(actividad);
                }
                if (listadoTareaPre.get(0).getEstadoPasoAprobacion() != null && listadoTareaPre.get(0).getEstadoPasoAprobacion().equals("1")) {
                    actividad = new ActividadPredeterminadaDTO();
                    UsuarioDTO usuario = consultas.getUserByCargoEmpresa(listadoTareaPre.get(0).getCargoAprobacion(), idEmpresa);
                    if (usuario != null && !usuario.getId().equals("")) {
                        actividad.setIdUsuarioPorCargo(usuario.getId());
                    } else {
                        UsuarioDTO usuarioContador = consultas.getUserByCargoEmpresaAndContador(listadoTareaPre.get(0).getCargoAprobacion(), empresa);
                        if (usuarioContador != null) {
                            actividad.setIdUsuarioPorCargo(usuarioContador.getId());
                        } else {
                            actividad.setIdUsuarioPorCargo(empresa.getIdContador());
                        }
                    }
                    actividad.setIdTarea(listadoTareaPre.get(0).getId());
                    actividad.setActividad(listadoTareaPre.get(0).getIdPasoAprobacion());
                    actividad.setDiasDuracion(listadoTareaPre.get(0).getDiasAprobacion());
                    actividad.setEstado(listadoTareaPre.get(0).getEstadoPasoAprobacion());
                    actividad.setDiasProrroga(listadoTareaPre.get(0).getProrrogaAprobacion());
                    actividad.setCargoActividad(listadoTareaPre.get(0).getCargoAprobacion());
                    actividad.setDescripcion("APROBACIÓN");
                    actividad.setOrdenActividad("3");

                    actividades.add(actividad);
                }

                if (listadoTareaPre.get(0).getEstadoPasoRecepcion() != null && listadoTareaPre.get(0).getEstadoPasoRecepcion().equals("1")) {
                    actividad = new ActividadPredeterminadaDTO();

                    UsuarioDTO usuario = consultas.getUserByCargoEmpresa(listadoTareaPre.get(0).getCargoRecepcion(), idEmpresa);
                    if (usuario != null && !usuario.getId().equals("")) {
                        actividad.setIdUsuarioPorCargo(usuario.getId());
                    } else {
                        UsuarioDTO usuarioContador = consultas.getUserByCargoEmpresaAndContador(listadoTareaPre.get(0).getCargoRecepcion(), empresa);
                        if (usuarioContador != null) {
                            actividad.setIdUsuarioPorCargo(usuarioContador.getId());
                        } else {
                            actividad.setIdUsuarioPorCargo(empresa.getIdContador());
                        }
                    }

                    actividad.setIdTarea(listadoTareaPre.get(0).getId());
                    actividad.setActividad(listadoTareaPre.get(0).getIdPasoRecepcion());
                    actividad.setDiasDuracion(listadoTareaPre.get(0).getDiasRecepcion());
                    actividad.setEstado(listadoTareaPre.get(0).getEstadoPasoRecepcion());
                    actividad.setDiasProrroga(listadoTareaPre.get(0).getProrrogaRecepcion());
                    actividad.setCargoActividad(listadoTareaPre.get(0).getCargoRecepcion());
                    actividad.setDescripcion("RECEPCIÓN");
                    actividad.setOrdenActividad("4");

                    actividades.add(actividad);
                }
//</editor-fold>
                if (!actividades.isEmpty()) {
                    listadoTareaPre.get(0).setActividades(actividades);

                    if (!ejecutar) {
                        listadoTareaPre = calculoFechasActividadesSiguientes(listadoTareaPre, empresa, fechaInicial);
                    } else {
                        ejecucion = new EjecucionTareaDTO();

                        ejecucion = llenarEjecucion(listadoTareaPre.get(0), fechaInicial, empresa, listadoDias);
                        listadoEjecucion = new ArrayList<EjecucionTareaDTO>();
                        listadoEjecucion.add(ejecucion);

                        listadoTareaPre.get(0).setFechaInicialReal(fechaInicial);
                        listadoTareaPre.get(0).setEjecuciones(listadoEjecucion);
                    }

                    if (listadoTareaPre != null) {

                        listadoTareaPre.get(0).setActividades(null);

                        //<editor-fold defaultstate="collapsed" desc="Registro actividades">
                        if (listadoTareaPre.get(0).getEjecuciones() != null) {

                            for (int j = 0; j < listadoTareaPre.get(0).getEjecuciones().size(); j++) {

                                ejecucionRegistrar = new EjecucionTareaDTO();
                                ejecucionRegistrar = listadoTareaPre.get(0).getEjecuciones().get(j);

                                boolean registroActividadEjecucion = false;

                                ultimaEjecucion = consultas.ultimaEjecucion(empresa.getId(), listadoTareaPre.get(0).getIdTareaEmpresa());
                                if (ejecucionRegistrar.getActividades() != null && !ultimaEjecucion.equals("")) {
                                    for (int k = 0; k < ejecucionRegistrar.getActividades().size(); k++) {
                                        ejecucionRegistrar.getActividades().get(k).setEstado("");

                                        if ((Integer.parseInt(ultimaEjecucion) == 1 && listadoTareaPre.get(0).getCalendarioFecha().equals("35")) || !listadoTareaPre.get(0).getCalendarioFecha().equals("35")) {

                                            registroActividadEjecucion = consultas.registrarEjecucionTarea(ejecucionRegistrar.getActividades().get(k), ultimaEjecucion, listadoTareaPre.get(0).getIdTareaEmpresa(), empresa.getIdContadorAsignado(), "prueba");

                                            if (registroActividadEjecucion) {

                                                tareaPro = consultas.listarEjecucionPorIdTareaEmpresa(listadoTareaPre.get(0).getIdTareaEmpresa());

                                                if (tareaPro != null) {
//                                                   System.out.println("tarea pro diferente de null, se va a cambiar la proxima  " + tareaPro.toStringJson());
                                                    tareaPro.setNueva("1");
                                                    int codigoEjecucion = (Integer.parseInt(tareaPro.getCodigoEjecucion()) + 1);
                                                    datosEjecucionProxima = calculoFechaEjecucionProxima2(tareaPro, empresa, tareaPro.getFechaInicialReal(), Integer.toString(codigoEjecucion));

                                                    if (datosEjecucionProxima != null) {
//                                                       System.out.println("datosEjescucionProxima diferente de null, se va a actualizar la proxima" + datosEjecucionProxima.toStringJson());

                                                        if (consultas.validarEjecucionProximaPorIdTareaEmpresa(datosEjecucionProxima.getIdTareaEmpresa()) != null) {
                                                            registroActividadEjecucion = consultas.actualizarEjecucionTareaProxima(datosEjecucionProxima, Integer.toString(codigoEjecucion));
                                                        } else {
                                                            registroActividadEjecucion = consultas.registrarEjecucionTareaProxima(datosEjecucionProxima);
                                                        }

                                                        if (!registroActividadEjecucion) {
                                                            throw new Exception("ERROR: no se pudo actualizar la ejecucion   tarea proxima ");
                                                        }
                                                    } else {
                                                        throw new Exception("ERROR: no se pudo registrar la fecha inicial de la proxima ejecucion   ");
                                                    }
                                                }
                                                // BORRAR este llamado debe borrarse porque ya no se llama segun el tpap
//                                                variable = new TareaPredeterminadaDAO().getIdTpAp( idTareaEmpresa, ejecucionRegistrar.getActividades().get(k).getActividad());

//                                                taemId = idTareaEmpresa;
                                                //Aca se debe buscar la tareapredeterminada_actividadpredeterminada 
                                                // para buscar el checklist y copiarlo  en  checklist_actividadempresa
                                                //System.out.println("  |ID tpap : " + variable);
                                                lista = consultas.obtenerCheckTareaEmpresa(idTareaEmpresa + "-" + ejecucionRegistrar.getActividades().get(k).getActividad());
                                                if (lista != null && !lista.isEmpty()) {

                                                    if (!registrarChecklist(lista, ejecucionRegistrar.getActividades().get(k).getId(), ejecucionRegistrar.getActividades().get(k).getActividad(), idTareaEmpresa)) {
                                                        System.out.println("  |ERROR: no se pudo registrar el checklist");
                                                        throw new Exception("ERROR: no se pudo registrar el checklist");
                                                    }
                                                } else {
                                                    // la lista de checklist es vacio puede qeu nunca se haya cargado la tarea entonces vamos averiguar
                                                    //si existe checklist para esta tarea empresa

//                                                    System.out.println("El listado de check es null o vacio ");
//                                                    System.out.println("idTareaEmpresa : " + idTareaEmpresa);
//                                                    System.out.println("idActividad : " + ejecucionRegistrar.getActividades().get(k).getActividad());
//                                                    System.out.println(":::::: " + ejecucionRegistrar.toStringJson());
                                                    variable = consultas.getIdTpAp(idTareaEmpresa, ejecucionRegistrar.getActividades().get(k).getActividad());
                                                    taemId = idTareaEmpresa;

                                                    if (!variable.equals("")) {
                                                        lista = consultas.obtener(variable);
                                                        if (lista != null && !lista.isEmpty()) {

                                                            //<editor-fold defaultstate="collapsed" desc="Bloque para copiar checklist de cada tarea a la tabla nueva">
                                                            // hacer metodo en DAO para copiar el checklist obtenido
                                                            if (registrarChecklistTareaEmpresa(lista, ejecucionRegistrar.getActividades().get(k).getActividad(), taemId)) {
//                                                                datosChecklist.put(taemId + "-" + ejecucionRegistrar.getActividades().get(k).getActividad(), lista);
                                                            }

//</editor-fold>
                                                            if (registrarChecklist(lista, ejecucionRegistrar.getActividades().get(k).getId(), ejecucionRegistrar.getActividades().get(k).getActividad(), taemId)) {
//                                                                datosChecklist.put(taemId + "-" + ejecucionRegistrar.getActividades().get(k).getActividad(), lista);
                                                            } else {
                                                                System.out.println(" | ERROR: no se pudo registrar el checklist_actividad emrpesa");
                                                                throw new Exception("ERROR: no se pudo registrar el checklist   ");
                                                            }
                                                        } else {
                                                            // Es vacio cuano el paso a realizar no tiene checklist
                                                            System.out.println(" | Este listado es vacio" + taemId + "-" + ejecucionRegistrar.getActividades().get(k).getActividad());
                                                        }

                                                    } else {
                                                        System.out.println("VARIABLE VACIA");
                                                    }

                                                }

                                            } else {
                                                throw new Exception("Error registrando actividad ejecucion");
                                            }
                                        }

                                    }
                                }

                            }

                            respuesta = new RespuestaVistaDTO();
                            respuesta.setMensaje("Se realizó el cargue correctamente ");
                            respuesta.setRegistro(true);

                            System.out.println("  |Se realizo el cargue correctamente de ejecuciones para el idTareaEmpresa: " + idTareaEmpresa);
                        }
//</editor-fold>

                    } else {
                        throw new Exception("Error : error ejecutando el cargue para la siguiente tarea empresa: " + idTareaEmpresa);
                    }

                } else {
                    throw new Exception("Error : error ejecutando el cargue para la siguiente tarea empresa: " + idTareaEmpresa);
                }

            } else {
                System.out.println("  | listado de tarea predetermianda es null ");
                respuesta = new RespuestaVistaDTO();
                respuesta.setMensaje("El listado de la tarea es null si la tarea es impuesto puede que este inactivo el impuesto");
                respuesta.setRegistro(true);
            }
            bandera = false;
        } catch (Exception e) {
            respuesta = new RespuestaVistaDTO();
            respuesta.setMensaje(e.getMessage());

            e.printStackTrace();
        } finally {
            if (!bandera && respuesta == null) {
                respuesta = new RespuestaVistaDTO();
                respuesta.setMensaje("error ejecutando el cargue para la siguiente tarea empresa");
                respuesta.setRegistro(false);
            }
        }

        return respuesta;
    }

    public static void actualizarLogTareasAtrasadas(ArrayList<EjecucionTareaDTO> listado, String idUsuario, String idContadorJuridico) {

//        System.out.println("  | actualizarLogTareasAtrasadas ");
        LogActividadesDTO ejecucion = null;
        String idEmpresaAnterior = "";
        String contadorEncargado = "";
        UsuarioDTO responsableAlertasEmpresa = null;
        ActividadPreDTO proximaEjecucion = null;
        boolean responsableEmpresa = false;
        try {

//            System.out.println("  |  Se actualizaron los estados de las ejecuciones atrasadas antes de listar las ejecuciones atrasadas");
            if (listado != null && !listado.isEmpty()) {
                for (int i = 0; i < listado.size(); i++) {
                    ejecucion = new LogActividadesDTO();

                    if (!consultas.obtenerLogPorIdEjecucion(idUsuario, listado.get(i).getId())) {
//                        System.out.println("  |no tiene log --->" + listado.get(i).getId());
                        ejecucion.setIdEjecucion(listado.get(i).getId());
                        ejecucion.setIdUsuarioDevuelve(idUsuario);

                        if (!idEmpresaAnterior.equals(listado.get(i).getIdEmpresa())) {
                            responsableAlertasEmpresa = consultas.consultarIdUsuarioResponsableAlertasPorIdEmpresa(listado.get(i).getIdEmpresa());

                            contadorEncargado = consultas.consultarIdContadorEncargado(listado.get(i).getIdEmpresa());
                        }

                        if (responsableAlertasEmpresa != null) {
                            proximaEjecucion = consultas.proximaEjecucionAjena(listado.get(i));
                            if (proximaEjecucion != null) {
                                if (!idUsuario.equals(proximaEjecucion.getIdUsuario())) {
                                    //se avisa al siguiente paso solo si es diferente al actual
                                    if (!proximaEjecucion.getIdUsuario().equals(responsableAlertasEmpresa.getId())) {
                                        ejecucion = new LogActividadesDTO();
                                        ejecucion.setIdEjecucion(listado.get(i).getId());
                                        ejecucion.setIdUsuarioDevuelve(proximaEjecucion.getIdUsuario());
                                        if (!consultas.obtenerLogPorIdEjecucion(proximaEjecucion.getIdUsuario(), listado.get(i).getId())) {
                                            if (consultas.registrarLogActividad(ejecucion, "3", "La actividad cambio de estado : ATRASADA.")) {
//                                            System.out.println("Se agrego log de atraso de actividad proximo encargado ==> 1");
                                            }

                                        }
                                    }
                                }

                                //Si el encargado de alertas y el proximo son el mismo, no se registra ningun alerta
                                if (!idUsuario.equals(responsableAlertasEmpresa.getId()) && idContadorJuridico == null) {
                                    //se avisa al encargado solo si es diferente al paso actual y no sea usuario con  coju_id
                                    ejecucion = new LogActividadesDTO();
                                    ejecucion.setIdEjecucion(listado.get(i).getId());
                                    ejecucion.setIdUsuarioDevuelve(responsableAlertasEmpresa.getId());
                                    if (!consultas.obtenerLogPorIdEjecucion(responsableAlertasEmpresa.getId(), listado.get(i).getId())) {
//                                    System.out.println(">> contadorEncargado : " + contadorEncargado);
//                                    System.out.println(">> proximaEjecucion  : " + proximaEjecucion.getId());

                                        if (consultas.registrarLogActividad(ejecucion, "3", "La actividad cambio de estado : ATRASADA.")) {
//                                        System.out.println("Se agrego log de atraso de actividad proximo encargado ==> 1");
                                        }
                                    }
                                } else if (!idUsuario.equals(contadorEncargado)) {
                                    ejecucion = new LogActividadesDTO();
                                    ejecucion.setIdEjecucion(listado.get(i).getId());
                                    ejecucion.setIdUsuarioDevuelve(contadorEncargado);
                                    if (!consultas.obtenerLogPorIdEjecucion(contadorEncargado, listado.get(i).getId())) {
//                                    System.out.println(">> contadorEncargado : " + contadorEncargado);
//                                    System.out.println(">> proximaEjecucion  : " + proximaEjecucion.getId());

                                        if (consultas.registrarLogActividad(ejecucion, "3", "La actividad cambio de estado : ATRASADA.")) {
//                                        System.out.println("Se agrego log de atraso de actividad proximo encargado ==> 1");
                                        }
                                    }
                                }

                            } else {
                                //si no hay proxima solo se avisa al encargado solo si no es el mismo al actual
                                if (!responsableAlertasEmpresa.getId().equals(idUsuario) && idContadorJuridico == null) {
                                    ejecucion = new LogActividadesDTO();
                                    ejecucion.setIdEjecucion(listado.get(i).getId());
                                    ejecucion.setIdUsuarioDevuelve(responsableAlertasEmpresa.getId());
                                    if (!consultas.obtenerLogPorIdEjecucion(responsableAlertasEmpresa.getId(), listado.get(i).getId())) {
                                        if (consultas.registrarLogActividad(ejecucion, "3", "La actividad cambio de estado : ATRASADA.")) {
//                                        System.out.println("  |Se agrego log de atraso de actividad proximo encargado ==> 4");
                                        }
                                    }
                                } else if (!contadorEncargado.equals(idUsuario)) {
                                    ejecucion = new LogActividadesDTO();
                                    ejecucion.setIdEjecucion(listado.get(i).getId());
                                    ejecucion.setIdUsuarioDevuelve(contadorEncargado);
                                    if (!consultas.obtenerLogPorIdEjecucion(contadorEncargado, listado.get(i).getId())) {
                                        if (consultas.registrarLogActividad(ejecucion, "3", "La actividad cambio de estado : ATRASADA.")) {
//                                        System.out.println("  |Se agrego log de atraso de actividad proximo encargado ==> 4");
                                        }
                                    }
                                }
                            }
                        } else {

                            proximaEjecucion = consultas.proximaEjecucionAjena(listado.get(i));
                            if (proximaEjecucion != null) {
                                if (!idUsuario.equals(proximaEjecucion.getIdUsuario())) {
                                    //se avisa al siguiente paso solo si es diferente al actual
                                    if (!proximaEjecucion.getIdUsuario().equals(contadorEncargado)) {
                                        ejecucion = new LogActividadesDTO();
                                        ejecucion.setIdEjecucion(listado.get(i).getId());
                                        ejecucion.setIdUsuarioDevuelve(proximaEjecucion.getIdUsuario());
                                        if (!consultas.obtenerLogPorIdEjecucion(proximaEjecucion.getIdUsuario(), listado.get(i).getId())) {
                                            if (consultas.registrarLogActividad(ejecucion, "3", "La actividad cambio de estado : ATRASADA.")) {
//                                            System.out.println("Se agrego log de atraso de actividad proximo encargado ==> 1");
                                            }

                                        }
                                    }
                                }

                                //Si el contador encargado y el proximo son el mismo, no se registra ningun alerta
                                if (!idUsuario.equals(contadorEncargado)) {
                                    //se avisa al contador solo si es diferente al paso actual
                                    ejecucion = new LogActividadesDTO();
                                    ejecucion.setIdEjecucion(listado.get(i).getId());
                                    ejecucion.setIdUsuarioDevuelve(contadorEncargado);
                                    if (!consultas.obtenerLogPorIdEjecucion(contadorEncargado, listado.get(i).getId())) {
//                                    System.out.println(">> contadorEncargado : " + contadorEncargado);
//                                    System.out.println(">> proximaEjecucion  : " + proximaEjecucion.getId());

                                        if (consultas.registrarLogActividad(ejecucion, "3", "La actividad cambio de estado : ATRASADA.")) {
//                                        System.out.println("Se agrego log de atraso de actividad proximo encargado ==> 1");
                                        }
                                    }
                                }

                            } else {
                                //si no hay proxima solo se avisa al contador solo si no es el mismo al actual
                                if (!contadorEncargado.equals(idUsuario)) {
                                    ejecucion = new LogActividadesDTO();
                                    ejecucion.setIdEjecucion(listado.get(i).getId());
                                    ejecucion.setIdUsuarioDevuelve(contadorEncargado);
                                    if (!consultas.obtenerLogPorIdEjecucion(contadorEncargado, listado.get(i).getId())) {
                                        if (consultas.registrarLogActividad(ejecucion, "3", "La actividad cambio de estado : ATRASADA.")) {
//                                        System.out.println("  |Se agrego log de atraso de actividad proximo encargado ==> 4");
                                        }
                                    }
                                }
                            }
                        }
                        idEmpresaAnterior = listado.get(i).getIdEmpresa();
                    } else {
                        // Si la alerta/log ya esta registrado y viene para volver a registrar solo le cambiamos el estado de leido)
                        if (consultas.cambiarEstadoAlertaNoLeido(idUsuario, listado.get(i).getId(), "0", "3")) {
//                            System.out.println("  |Se cambio de estado una alerta de tarea atrasada para volverse a mostrar ==> 5");
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // return listado;
    }

    public static ArrayList<TareaPredeterminadaDTO> calculoFechasActividadesSiguientes(ArrayList<TareaPredeterminadaDTO> listado, EmpresaDTO empresa, String fechaFinal) {
//        System.out.println("  | calculoFechasActividadesSiguientes ");
        try {
            String fechaInicialCalendario = "";
            String fechaFinalCalendario = "";

            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date fechaIniDate = null;
            Calendar calendar = Calendar.getInstance();
            int diasTarea = 0;
            boolean fechafinal = false;
            boolean diaHabilSiguiente;
            Calendar calendarFechaFinal = Calendar.getInstance();
            calendarFechaFinal.setTime(formato.parse(fechaFinal));

            Calendar calendarMensual = Calendar.getInstance();
            calendarMensual.setTime(formato.parse(fechaFinal));
            calendarMensual.add(Calendar.MONTH, 1);

            Calendar calendarAnual = Calendar.getInstance();
            calendarAnual.setTime(formato.parse(fechaFinal));
            calendarAnual.set(Calendar.YEAR, calendarAnual.get(Calendar.YEAR) + 1);

            for (int i = 0; i < listado.size(); i++) {
                //<editor-fold defaultstate="collapsed" desc="Tarea es un impuesto">
                if (listado.get(i).getImpuesto().equals("1")) {

                    if (listado.get(i).getFechaCalendarioTributario() != null && !listado.get(i).getFechaCalendarioTributario().equals("")) {

                        fechaInicialCalendario = fechaInicialHabilTarea(listado.get(i).getFechaCalendarioTributario(), empresa);

                        //fechaIniDate = formato.parse(fechaInicialCalendario);
                        //calendar.setTime(fechaIniDate);
                        //System.out.println("  |Fecha final   calendario tributario" + listado.get(i).getFechaCalendarioTributario());
                        //System.out.println("  |Fecha final  habiles para calendario tributario" + fechaInicialCalendario);
                        //Aca enviamos el 3 porque son los dias antes de la fecha final del calendario tributario
                        fechaInicialCalendario = calculoDeFechaCompletoImpuesto(fechaInicialCalendario, 3, empresa, fechafinal);

                        fechaInicialCalendario = fechaInicialHabilTarea(fechaInicialCalendario, empresa);
                        listado.get(i).setFechaFinalTusCuentas(fechaInicialCalendario);
                        // System.out.println("  |Fecha final dias habiles para tus cuentas" + fechaInicialCalendario);

                        for (int j = 0; j < listado.get(i).getActividades().size(); j++) {
                            diasTarea += Integer.parseInt(listado.get(i).getActividades().get(j).getDiasDuracion());
                            diasTarea += Integer.parseInt(listado.get(i).getActividades().get(j).getDiasProrroga());
                        }
                        fechaInicialCalendario = calculoDeFechaCompletoImpuesto(fechaInicialCalendario, diasTarea, empresa, fechafinal);
                        for (int j = 0; j < listado.get(i).getActividades().size(); j++) {

                            listado.get(i).getActividades().get(j).setFechaInicial(fechaInicialCalendario);
                            fechaFinalCalendario = calculoDeFechaCompletoImpuesto(fechaInicialCalendario, (Integer.parseInt(listado.get(i).getActividades().get(j).getDiasDuracion()) - 1), empresa, !fechafinal);
                            listado.get(i).getActividades().get(j).setFechaFinal(fechaFinalCalendario);

                            //fechaInicialCalendario = fechaFinalCalendario;
                            //if (j > 0) {
                            fechaIniDate = formato.parse(fechaFinalCalendario);
                            calendar.setTime(fechaIniDate);
                            calendar.add(Calendar.DAY_OF_YEAR, 1);
                            fechaIniDate = calendar.getTime();
                            fechaFinalCalendario = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));
                            fechaFinalCalendario = (calculoDeFechaCompleto(fechaFinalCalendario, 1, empresa, true));

                            //}
                            fechaInicialCalendario = fechaFinalCalendario;

                        }

                        listado.get(i).setDuracionConProrroga(diasTarea);
                        //listado.get(i).setEjecuciones(listadoEjecucion);
                        diasTarea = 0;
                        //System.out.println("  |Tarea con listado de actividades y cantidad de dias y fecha final " + i + " :: " + listado.get(i).toStringJson());

                    } else {
                        throw new Exception("La tarea es impuesto pero notiene fecha en calendario tributario idTarea : " + listado.get(i).getId());
                    }

                } //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="Tarea no es un impuesto">
                else {
                    //La tarea no es un impuetso se procede a calcular las fechas con el calendario fechas

                    Calendar c = Calendar.getInstance();
                    // cantidad de dias que trae el mes
                    int diasMes = calcularDiasMes((c.get(Calendar.MONTH) + 1), c.get(Calendar.YEAR));
                    // cantidad de dias que faltan para que se acabe el mes
                    diasMes -= c.get(Calendar.DATE);

                    diaHabilSiguiente = !listado.get(i).getDiaHabilSiguiente().equals("0");

                    System.out.println("  |calendario fecha es ===== " + listado.get(i).getCalendarioFecha());

                    switch (listado.get(i).getCalendarioFecha()) {

                        case "1":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_DIARIA);
                            listado.get(i).setEjecuciones(recurrenteDiarioSiguiente(empresa, diasMes, listado.get(i), fechaFinal));

                            break;

                        //<editor-fold defaultstate="collapsed" desc="Recurrencia Mensual aleatoria ">
                        // se pone para cada primero de cada mes 
                        case "2":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                            recurrenteMensualSiguientes(empresa, 9, diaHabilSiguiente, listado.get(i), calendarMensual);

                            break;

                        //</editor-fold>  
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia semanal los lunes ">
                        case "3":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_SEMANAL);

                            /**
                             * en la tarea debe quedar el listado de ejecucione
                             * se envia el 2 porqeu es recurrente todos los
                             * lunes
                             */
                            listado.get(i).setEjecuciones(recurrentesemanalSiguientes(empresa, diasMes, 2, listado.get(i), calendarFechaFinal));

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia semanal los viernes">
                        case "4":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_SEMANAL);
                            /**
                             * en la tarea debe quedar el listado de ejecucione
                             * se envia el 6 porqeu es recurrente todos los
                             * viernes
                             */
                            listado.get(i).setEjecuciones(recurrentesemanalSiguientes(empresa, diasMes, 6, listado.get(i), calendarFechaFinal));

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual antepenultimo dia del mes ">
                        case "5":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                            int diasDelMes = calcularDiasMes((calendarFechaFinal.get(Calendar.MONTH) + 1), calendarFechaFinal.get(Calendar.YEAR));
                            diasDelMes -= 2;
                            recurrenteMensualSiguientes(empresa, diasDelMes, diaHabilSiguiente, listado.get(i), calendarMensual);
                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 1 del mes">
                        case "6":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                            recurrenteMensualSiguientes(empresa, 1, diaHabilSiguiente, listado.get(i), calendarMensual);

                            break;
                        //</editor-fold>  
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia mensaual dia 2 del mes">
                        case "7":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                            recurrenteMensualSiguientes(empresa, 2, diaHabilSiguiente, listado.get(i), calendarMensual);

                            break;
                        //</editor-fold>    
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 3 del mes ">
                        case "8":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                            recurrenteMensualSiguientes(empresa, 3, diaHabilSiguiente, listado.get(i), calendarMensual);

                            break;

                        //</editor-fold>                           
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 4 del mes ">
                        case "9":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
//                            System.out.println("  |::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
                            recurrenteMensualSiguientes(empresa, 4, diaHabilSiguiente, listado.get(i), calendarMensual);
                            break;
                        //</editor-fold>                      
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 5 del mes">
                        case "10":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                            recurrenteMensualSiguientes(empresa, 5, diaHabilSiguiente, listado.get(i), calendarMensual);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 10 del mes">
                        case "11":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                            recurrenteMensualSiguientes(empresa, 10, diaHabilSiguiente, listado.get(i), calendarMensual);

                            break;
                        //</editor-fold>                                                                                 
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 25 del mes">
                        case "12":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                            if (calendarFechaFinal.get(Calendar.DAY_OF_MONTH) > 25) {
                                recurrenteMensualSiguientes(empresa, 25, diaHabilSiguiente, listado.get(i), calendarMensual);
                            } else {
                                recurrenteMensualSiguientes(empresa, 25, diaHabilSiguiente, listado.get(i), calendarFechaFinal);
                            }

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 27 del mes">
                        case "13":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                            if (calendarFechaFinal.get(Calendar.DAY_OF_MONTH) > 27) {
                                recurrenteMensualSiguientes(empresa, 27, diaHabilSiguiente, listado.get(i), calendarMensual);
                            } else {
                                recurrenteMensualSiguientes(empresa, 27, diaHabilSiguiente, listado.get(i), calendarFechaFinal);
                            }

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual ultmo dia del mes">
                        case "14":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);

                            int ultimoDiaMes = calcularDiasMes((calendarFechaFinal.get(Calendar.MONTH) + 1), calendarFechaFinal.get(Calendar.YEAR));

                            recurrenteMensualSiguientes(empresa, ultimoDiaMes, diaHabilSiguiente, listado.get(i), calendarFechaFinal);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 1 de enero  ... 1=dia del mes ;  0 = enero segun calendar">
                        case "15":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 1, 0, diaHabilSiguiente, listado.get(i), calendarAnual);
                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 5 de enero">
                        case "16":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 5, 0, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 6 de enero">
                        case "17":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 6, 0, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 15 de enero">
                        case "18":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 15, 0, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrecia anual dia 30 de enero ">
                        case "19":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 30, 0, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 15 de febrero">
                        case "20":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 15, 1, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 16de febrero">
                        case "21":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 16, 1, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 17 de febrero">
                        case "22":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 17, 1, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual ultimo dia de febrero">
                        case "23":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            int ultimoDiaMesFebrero = calcularDiasMes((2), c.get(Calendar.YEAR));
                            recurrenteAnualSiguientes(empresa, ultimoDiaMesFebrero, 1, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 1 de marzo">
                        case "24":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 1, 2, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>    

                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 5 de marzo">
                        case "25":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 5, 2, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 15 de marzo">
                        case "26":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 15, 2, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual ultimo dia marzo">
                        case "27":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 31, 2, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 1 de agosto">
                        case "28":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 1, 7, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 5 de agosto">
                        case "29":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 5, 7, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 15 de agosto">
                        case "30":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 15, 7, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>  
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia Cuando aplique">

                        case "31":
                            listado.get(i).setRecurrente("0");

                            //System.out.println("  |La tarea tiene calendario fehca 31  :: " + listado.get(i).toStringJson());
                            //System.out.println("  |\nLa fecha es cuando aplque en calendario fechas");
                            break;
                        //</editor-fold>  
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 1 septiembre">
                        case "32":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 1, 8, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
//</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 15 septiembre">
                        case "33":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 15, 8, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 1 octubre">
                        case "34":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 1, 9, diaHabilSiguiente, listado.get(i), calendarAnual);

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 1 octubre">
                        case "35":
                            System.out.println("  |Esta tarea no se lanza debido a que solo tiene una tarea en su historia.");
                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia semanal los martes ">
                        case "36":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_SEMANAL);

                            /**
                             * en la tarea debe quedar el listado de ejecucione
                             * se envia el 3 porqeu es recurrente todos los
                             * martes
                             */
                            listado.get(i).setEjecuciones(recurrentesemanalSiguientes(empresa, diasMes, 3, listado.get(i), calendarFechaFinal));

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia semanal los miercoles ">
                        case "37":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_SEMANAL);

                            /**
                             * en la tarea debe quedar el listado de ejecucione
                             * se envia el 4 porqeu es recurrente todos los
                             * miercoles
                             */
                            listado.get(i).setEjecuciones(recurrentesemanalSiguientes(empresa, diasMes, 4, listado.get(i), calendarFechaFinal));

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia semanal los jueves ">
                        case "38":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_SEMANAL);

                            /**
                             * en la tarea debe quedar el listado de ejecucione
                             * se envia el 5 porqeu es recurrente todos los
                             * jueves
                             */
                            listado.get(i).setEjecuciones(recurrentesemanalSiguientes(empresa, diasMes, 5, listado.get(i), calendarFechaFinal));

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 15 del mes">
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia semanal los sabados ">
                        case "39":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_SEMANAL);

                            /**
                             * en la tarea debe quedar el listado de ejecucione
                             * se envia el 7 porqeu es recurrente todos los
                             * sabados
                             */
                            listado.get(i).setEjecuciones(recurrentesemanalSiguientesSabadoDomingo(empresa, diasMes, 7, listado.get(i), calendarFechaFinal));

                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia semanal los domingos ">
                        case "40":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_SEMANAL);

                            /**
                             * en la tarea debe quedar el listado de ejecucione
                             * se envia el 1 porqeu es recurrente todos los
                             * domingos
                             */
                            listado.get(i).setEjecuciones(recurrentesemanalSiguientesSabadoDomingo(empresa, diasMes, 1, listado.get(i), calendarFechaFinal));

                            break;
                        //</editor-fold>
                        case "41":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                            recurrenteMensualSiguientes(empresa, 15, diaHabilSiguiente, listado.get(i), calendarMensual);

                            break;
                        //</editor-fold>  
                        //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 1 de enero  ... 1=dia del mes ;  0 = enero segun calendar">
                        case "42":
                            listado.get(i).setRecurrente("1");
                            listado.get(i).setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                            recurrenteAnualSiguientes(empresa, 1, 1, diaHabilSiguiente, listado.get(i), calendarAnual);
                            break;
                        //</editor-fold>
                        //<editor-fold defaultstate="collapsed" desc="Cuando aplique">
                        default:
                            //System.out.println("  |\nLa fecha es cuando aplque en calendario fechas");
                            break;
                        //</editor-fold>

                    }

                }
//</editor-fold>

            }

//            System.out.println("  |Liestado de tareas");
//
//            for (int i = 0; i < listado.size(); i++) {
//
//                System.out.println("  |Tarea final a registrar::: " + listado.get(i).toStringJson());
//
//            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        return listado;

    }

    public static String fechaInicialHabilTarea(String fechaInicial, EmpresaDTO empresa) {

        try {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

            java.util.Date fechaIniDate = null;
            Calendar calendar = Calendar.getInstance();

            boolean fechaCorrecta = true;
            while (fechaCorrecta) {
                fechaIniDate = formato.parse(fechaInicial);
                calendar.setTime(fechaIniDate);
                int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);

                if (empresa.getDialaboralSabado().equals("1") && diaSemana == 7) {
                } else if (empresa.getDialaboralDomingo().equals("1") && diaSemana == 1) {
                } else if (empresa.getDialaboralFestivos().equals("1") && listadoDias.contains(fechaInicial)) {
                } else {
                    break;
                }
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                fechaIniDate = calendar.getTime();
                fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
        return fechaInicial;

    }

    public static EjecucionTareaDTO llenarEjecucion(TareaPredeterminadaDTO tarea, String fechainic, EmpresaDTO empresa, ArrayList<String> listadoDias) throws ParseException {
        java.util.Date fechaIniDate = null;
        ActividadPredeterminadaDTO actividad = null;
        ActividadPredeterminadaDTO actividadPlantilla = null;
        ArrayList<ActividadPredeterminadaDTO> listadoActividades = null;
        EjecucionTareaDTO ejecucion = null;
        Calendar calendar = null;
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
//        System.out.println("  |");

        //String fechainic = Generales.EMPTYSTRING;
        boolean fechaFinal = true;
//        System.out.println(" | ");
//        System.out.println(" | fechainic: " + fechainic);
        try {
            calendar = Calendar.getInstance();
            //fechainic = fechaInicial;
            listadoActividades = new ArrayList<>();
            ejecucion = new EjecucionTareaDTO();
            // String fechaInicial = formato.format(calendar.getTime());

            for (int i = 0; i < tarea.getActividades().size(); i++) {

                actividadPlantilla = tarea.getActividades().get(i);
                actividad = new ActividadPredeterminadaDTO();
                actividad.setDescripcion(actividadPlantilla.getDescripcion());
                actividad.setOrdenActividad(actividadPlantilla.getOrdenActividad());
                actividad.setCargoActividad(actividadPlantilla.getCargoActividad());
                actividad.setDiasDuracion(actividadPlantilla.getDiasDuracion());
                actividad.setDiasProrroga(actividadPlantilla.getDiasProrroga());
                actividad.setActividad(actividadPlantilla.getActividad());
                actividad.setIdUsuarioPorCargo(actividadPlantilla.getIdUsuarioPorCargo());
                actividad.setEstado(actividadPlantilla.getEstado());

                fechaIniDate = formato.parse(fechainic);
                calendar.setTime(fechaIniDate);
                actividad.setFechaInicial(fechainic);
                if (i > 0) {
                    calendar.setTime(fechaIniDate);
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                    fechaIniDate = calendar.getTime();
                    fechainic = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));
                    actividad.setFechaInicial(calculoDeFechaCompleto(fechainic, 1, empresa, true));
                    //actividad.setFechaInicial(fechainic);
                }
                // se envia la duracion de dias sin prorroga
                actividad.setFechaFinal(calculoDeFechaCompleto(fechainic, Integer.parseInt(actividadPlantilla.getDiasDuracion()), empresa, fechaFinal));

                //calendar.add(Calendar.DAY_OF_YEAR, 1);
                fechainic = actividad.getFechaFinal();

                listadoActividades.add(actividad);

            }

            ejecucion.setActividades(listadoActividades);

        } catch (Exception e) {
            e.printStackTrace();
        }

        //ejecucion.setIdTarea(tarea.getId());
        // System.out.println("  |Ejecucion=========================  " + ejecucion.toStringJson());
        return ejecucion;

    }

    public static String calculoDeFechaCompleto(String fechaInicial, int cantDias, EmpresaDTO empresa, boolean fechafinal) {

        try {
            //listadoDias = ();
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

            java.util.Date fechaIniDate = null;
            Calendar calendar = Calendar.getInstance();

            boolean fechaValida = true;
            while (fechaValida) {

                fechaIniDate = formato.parse(fechaInicial);
                calendar.setTime(fechaIniDate);
                int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);

                if (empresa.getDialaboralSabado().equals("1") && diaSemana == 7) {
                    cantDias++;
                } else if (empresa.getDialaboralDomingo().equals("1") && diaSemana == 1) {
                    cantDias++;
                } else if (empresa.getDialaboralFestivos().equals("1") && listadoDias.contains(fechaInicial)) {
                    cantDias++;
                } else {
                    if (cantDias == 0) {
                        break;
                    }
                }
                if (cantDias > 1) {
                    if (fechafinal) {
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                    } else {
                        calendar.add(Calendar.DAY_OF_YEAR, -1);
                    }
                }

                fechaIniDate = calendar.getTime();
                fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));
                cantDias--;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
        return fechaInicial;

    }

    public static EjecucionProximaDTO calculoFechaEjecucionProxima2(TareaPredeterminadaDTO datos, EmpresaDTO empresa, String fechaFinal, String codEjecucion) {
        //System.out.println("  |  ");
        ArrayList<String> listadoDias = null;
        EjecucionProximaDTO datosResponse = new EjecucionProximaDTO();

        try {
            String fechaInicialCalendario = "";

            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            int diasTarea = 0;
            boolean diaHabilSiguiente;

            Calendar calendarFechaFinal = Calendar.getInstance();
            calendarFechaFinal.setTime(formato.parse(fechaFinal));

            Calendar calendarMensual = Calendar.getInstance();
            calendarMensual.setTime(formato.parse(fechaFinal));
            calendarMensual.set(Calendar.MONTH, calendarMensual.get(Calendar.MONTH) + 1);

            Calendar calendarAnual = Calendar.getInstance();
            calendarAnual.setTime(formato.parse(fechaFinal));
            calendarAnual.set(Calendar.YEAR, calendarAnual.get(Calendar.YEAR));

            Calendar calendarGeneric = Calendar.getInstance();
            Calendar calendarHoy = Calendar.getInstance();

            //<editor-fold defaultstate="collapsed" desc="Tarea es un impuesto">
            if (datos.getImpuesto().equals("1")) {
                if (datos.getFechaInicialReal() != null && !datos.getFechaInicialReal().equals("") && datos.getRecurrente() != null) {

                    //<editor-fold defaultstate="collapsed" desc="Recurrencia impuesto">
                    System.out.println("  | Real : " + datos.getFechaInicialReal());
                    System.out.println("  | Real recurrente : " + datos.getRecurrente());

                    calendarGeneric.setTime(formato.parse(datos.getFechaInicialReal()));
                    calendarGeneric.set(Calendar.DAY_OF_MONTH, Integer.parseInt(datos.getDiasMes()));

                    switch (datos.getRecurrente()) {

                        case "1":// recurrencia anual
                            calendarGeneric.add(Calendar.MONTH, 12);
                            break;
                        case "2": // recurrencia mensual
                            calendarGeneric.add(Calendar.MONTH, 1);
                            break;
                        case "3": // recurrencia bimestral
                            calendarGeneric.add(Calendar.MONTH, 2);
                            break;
                        case "4":// recurrencia trimestral
                            calendarGeneric.add(Calendar.MONTH, 3);
                            break;
                        case "5":// recurrencia semestral
                            calendarGeneric.add(Calendar.MONTH, 6);
                            break;
                        case "6":// recurrencia semestral
                            calendarGeneric.add(Calendar.MONTH, 4);
                            break;
                        default:
                            throw new AssertionError();
                    }
                    calendarGeneric.add(Calendar.DAY_OF_MONTH, -Integer.parseInt(datos.getDiasAntes()));

//</editor-fold>
                    fechaInicialCalendario = fechaInicialHabilTarea(formato.format(calendarGeneric.getTime()), empresa);
                    System.out.println("  | Real >>  : " + fechaInicialCalendario);

                    datos.setFechaInicialReal(fechaInicialCalendario);

                    datos.setDuracionConProrroga(diasTarea);
                    //listado.get(i).setEjecuciones(listadoEjecucion);
                    diasTarea = 0;
                    //System.out.println("  |Tarea con listado de actividades y cantidad de dias y fecha final " + i + " :: " + listado.get(i).toStringJson());

                } else {
                    // la tarea es impuesto y no tiene fecha en calendario tributario pero eso no debe parar el proceso
                    System.out.println("La tarea es impuesto pero notiene fecha en calendario tributario idTarea : " + datos.getId());
                }

            } //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Tarea no es un impuesto">
            else {
                //La tarea no es un impuetso se procede a calcular las fechas con el calendario fechas
                Calendar c = Calendar.getInstance();
                // cantidad de dias que trae el mes
                int diasMes = calcularDiasMes((c.get(Calendar.MONTH) + 1), c.get(Calendar.YEAR));
                // cantidad de dias que faltan para que se acabe el mes
                diasMes -= c.get(Calendar.DATE);

                diaHabilSiguiente = !datos.getDiaHabilSiguiente().equals("0");
                switch (datos.getCalendarioFecha()) {

                    case "1":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_DIARIA);
                        datos.setFechaInicialReal(recurrenteDiarioGenerarProxima(empresa, diasMes, datos, fechaFinal));

                        break;

                    //<editor-fold defaultstate="collapsed" desc="Recurrencia Mensual aleatoria ">
                    // se pone para cada primero de cada mes 
                    case "2":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                        recurrenteMensualGenerarProximas(empresa, 9, diaHabilSiguiente, datos, calendarFechaFinal);

                        break;

                    //</editor-fold>  
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia semanal los lunes ">
                    case "3":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_SEMANAL);

                        /**
                         * en la tarea debe quedar el listado de ejecucione se
                         * envia el 2 porqeu es recurrente todos los lunes
                         */
                        datos.setFechaInicialReal(recurrentesemanalGenerarProximas(empresa, diasMes, 2, datos, calendarFechaFinal, diaHabilSiguiente));

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia semanal los viernes">
                    case "4":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_SEMANAL);
                        /**
                         * en la tarea debe quedar el listado de ejecucione se
                         * envia el 6 porqeu es recurrente todos los viernes
                         */
                        datos.setFechaInicialReal(recurrentesemanalGenerarProximas(empresa, diasMes, 6, datos, calendarFechaFinal, diaHabilSiguiente));

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual antepenultimo dia del mes ">
                    case "5":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                        int diasDelMes = calcularDiasMes((calendarMensual.get(Calendar.MONTH) + 1), calendarMensual.get(Calendar.YEAR));
                        diasDelMes -= 2;
                        recurrenteMensualGenerarProximas(empresa, diasDelMes, diaHabilSiguiente, datos, calendarFechaFinal);
                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 1 del mes">
                    case "6":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                        recurrenteMensualGenerarProximas(empresa, 1, diaHabilSiguiente, datos, calendarFechaFinal);

                        break;
                    //</editor-fold>  
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 2 del mes">
                    case "7":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                        recurrenteMensualGenerarProximas(empresa, 2, diaHabilSiguiente, datos, calendarFechaFinal);

                        break;
                    //</editor-fold>    
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 3 del mes ">
                    case "8":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                        recurrenteMensualGenerarProximas(empresa, 3, diaHabilSiguiente, datos, calendarFechaFinal);

                        break;

                    //</editor-fold>                           
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 4 del mes ">
                    case "9":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
//                            System.out.println("  |::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
                        recurrenteMensualGenerarProximas(empresa, 4, diaHabilSiguiente, datos, calendarFechaFinal);
                        break;
                    //</editor-fold>                      
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 5 del mes">
                    case "10":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                        recurrenteMensualGenerarProximas(empresa, 5, diaHabilSiguiente, datos, calendarFechaFinal);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 10 del mes">
                    case "11":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                        recurrenteMensualGenerarProximas(empresa, 10, diaHabilSiguiente, datos, calendarFechaFinal);

                        break;
                    //</editor-fold>                                                                                 
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 25 del mes">
                    case "12":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                        if (calendarFechaFinal.get(Calendar.DAY_OF_MONTH) > 25) {
//                            System.out.println("  |entra al if del caso 12");
                            recurrenteMensualSiguientes(empresa, 25, diaHabilSiguiente, datos, calendarFechaFinal);
                        } else {
//                            System.out.println("  |No entra al if del caso 12");
                            recurrenteMensualGenerarProximas(empresa, 25, diaHabilSiguiente, datos, calendarFechaFinal);
                        }

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 27 del mes">
                    case "13":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                        if (calendarFechaFinal.get(Calendar.DAY_OF_MONTH) > 27) {
                            recurrenteMensualGenerarProximas(empresa, 27, diaHabilSiguiente, datos, calendarFechaFinal);
                        } else {
                            recurrenteMensualGenerarProximas(empresa, 27, diaHabilSiguiente, datos, calendarFechaFinal);
                        }

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual ultmo dia del mes">
                    case "14":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);

                        int ultimoDiaMes = calcularDiasMes((calendarMensual.get(Calendar.MONTH) + 1), calendarMensual.get(Calendar.YEAR));

                        recurrenteMensualGenerarProximas(empresa, ultimoDiaMes, diaHabilSiguiente, datos, calendarFechaFinal);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 1 de enero  ... 1=dia del mes ;  0 = enero segun calendar">
                    case "15":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 1, 0, diaHabilSiguiente, datos, calendarAnual);
                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 5 de enero">
                    case "16":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 5, 0, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 6 de enero">
                    case "17":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 6, 0, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 15 de enero">
                    case "18":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 15, 0, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrecia anual dia 30 de enero ">
                    case "19":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 30, 0, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 15 de febrero">
                    case "20":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 15, 1, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 16de febrero">
                    case "21":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 16, 1, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 17 de febrero">
                    case "22":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 17, 1, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual ultimo dia de febrero">
                    case "23":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        int ultimoDiaMesFebrero = calcularDiasMes((2), c.get(Calendar.YEAR));
                        recurrenteAnualGenerarProximas(empresa, ultimoDiaMesFebrero, 1, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 1 de marzo">
                    case "24":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 1, 2, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>    

                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 5 de marzo">
                    case "25":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 5, 2, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 15 de marzo">
                    case "26":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 15, 2, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual ultimo dia marzo">
                    case "27":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 31, 2, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 1 de agosto">
                    case "28":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 1, 7, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 5 de agosto">
                    case "29":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 5, 7, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 15 de agosto">
                    case "30":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 15, 7, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>  
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia Cuando aplique">

                    case "31":
                        datos.setRecurrente("0");

                        //System.out.println("  |La tarea tiene calendario fehca 31  :: " + listado.get(i).toStringJson());
                        //System.out.println("  |\nLa fecha es cuando aplque en calendario fechas");
                        break;
                    //</editor-fold>  
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 1 septiembre">
                    case "32":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 1, 8, diaHabilSiguiente, datos, calendarAnual);

                        break;
//</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 15 septiembre">
                    case "33":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 15, 8, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 1 octubre">
                    case "34":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 1, 9, diaHabilSiguiente, datos, calendarAnual);

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 1 octubre">
                    case "35":
                        System.out.println("  |Esta tarea no se lanza debido a que solo tiene una tarea en su historia.");
                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia semanal los martes ">
                    case "36":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_SEMANAL);

                        /**
                         * en la tarea debe quedar el listado de ejecucione se
                         * envia el 3 porqeu es recurrente todos los martes
                         */
                        datos.setFechaInicialReal(recurrentesemanalGenerarProximas(empresa, diasMes, 3, datos, calendarFechaFinal, diaHabilSiguiente));

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia semanal los miercoles ">
                    case "37":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_SEMANAL);

                        /**
                         * en la tarea debe quedar el listado de ejecucione se
                         * envia el 4 porqeu es recurrente todos los miercoles
                         */
                        datos.setFechaInicialReal(recurrentesemanalGenerarProximas(empresa, diasMes, 4, datos, calendarFechaFinal, diaHabilSiguiente));

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia semanal los jueves ">
                    case "38":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_SEMANAL);

                        /**
                         * en la tarea debe quedar el listado de ejecucione se
                         * envia el 5 porque es recurrente todos los jueves
                         */
                        datos.setFechaInicialReal(recurrentesemanalGenerarProximas(empresa, diasMes, 5, datos, calendarFechaFinal, diaHabilSiguiente));

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia semanal los sabados ">
                    case "39":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_SEMANAL);

                        /**
                         * en la tarea debe quedar el listado de ejecucione se
                         * envia el 7 porqeu es recurrente todos los sabados
                         */
                        datos.setFechaInicialReal(recurrentesemanalSabadoDomingoGenerarProximas(empresa, diasMes, 7, datos, calendarFechaFinal));

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia semanal los domingos ">
                    case "40":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_SEMANAL);

                        /**
                         * en la tarea debe quedar el listado de ejecucione se
                         * envia el 1 porque es recurrente todos los domingos
                         */
                        datos.setFechaInicialReal(recurrentesemanalSabadoDomingoGenerarProximas(empresa, diasMes, 1, datos, calendarFechaFinal));

                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia mensual dia 15 del mes">
                    case "41":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_MENSUAL);
                        recurrenteMensualGenerarProximas(empresa, 15, diaHabilSiguiente, datos, calendarFechaFinal);

                        break;
                    //</editor-fold> 
                    //<editor-fold defaultstate="collapsed" desc="Recurrencia anual dia 1 de febrero  ... 1=dia del mes ;  1 = febrero segun calendar">
                    case "42":
                        datos.setRecurrente("1");
                        datos.setTipoRecurrencia(Constantes.RECURRENCIA_ANUAL);
                        recurrenteAnualGenerarProximas(empresa, 1, 1, diaHabilSiguiente, datos, calendarAnual);
                        break;
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Cuando aplique">
                    default:
                        //System.out.println("  |\nLa fecha es cuando aplque en calendario fechas");
                        break;
                    //</editor-fold>

                }

            }

//</editor-fold>
            datosResponse.setCodigoEjecucionProxima(codEjecucion);
            datosResponse.setFechaInicialProxima(datos.getFechaInicialReal());
            datosResponse.setIdTareaEmpresa(datos.getIdTareaEmpresa());
            datosResponse.setCalendarioFecha(datos.getCalendarioFecha());
        } catch (Exception e) {
            e.printStackTrace();

        }

        return datosResponse;

    }

    public static boolean registrarChecklist(ArrayList<CheckListDTO> lista, String idEjecucion, String idActividad, String idTareaEmpresa) {

        boolean respuesta = false;
        try {

            if (lista != null && !lista.isEmpty()) {
                for (int e = 0; e < lista.size(); e++) {
                    CheckListDTO checklist = new CheckListDTO();
                    checklist.setOrden(lista.get(e).getOrden());
                    checklist.setDescripcion(idTareaEmpresa + "-" + idActividad);

                    if (consultas.registrarChecklistActividad(checklist, idEjecucion)) {
                        respuesta = true;
                    } else {
                        System.out.println("  |ERROR: no se pudo registrar el checklist");
                        respuesta = false;
                        throw new Exception("ERROR: no se pudo registrar el checklist   ");
                    }
                }
                //datosChecklist.put(taemId + "-" + ejecucionRegistrar.getActividades().get(k).getActividad(), lista);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return respuesta;
    }

    public static boolean registrarChecklistTareaEmpresa(ArrayList<CheckListDTO> lista, String idActividad, String idTareaEmpresa) {

        boolean respuesta = false;
        try {

            if (lista != null && !lista.isEmpty()) {
                for (int e = 0; e < lista.size(); e++) {
                    String datos = consultas.obtenerOrdenNuevoCheck(idTareaEmpresa + "-" + idActividad);
                    CheckListDTO checklist = new CheckListDTO();
                    checklist.setOrden(datos);
                    checklist.setAyuda(lista.get(e).getAyuda());
                    checklist.setDescripcion(lista.get(e).getDescripcion());
//                    checklist.setDescripcion(lista.get(e).getIdCheck());

                    if (consultas.registrarCheckListTareaEmpresa(checklist, idTareaEmpresa, idActividad).isRegistro()) {
                        respuesta = true;
                    } else {
                        respuesta = false;
                        throw new Exception("ERROR: no se pudo registrar el checklist   ");
                    }
                }
                //datosChecklist.put(taemId + "-" + ejecucionRegistrar.getActividades().get(k).getActividad(), lista);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return respuesta;
    }

    public static String calculoDeFechaCompletoImpuesto(String fechaInicial, int cantDias, EmpresaDTO empresa, boolean fechafinal) {

        try {
            //listadoDias = litarDiasFestivos();
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

            java.util.Date fechaIniDate = null;
            Calendar calendar = Calendar.getInstance();

            boolean fechaValida = true;
            while (fechaValida) {

                fechaIniDate = formato.parse(fechaInicial);
                calendar.setTime(fechaIniDate);
                int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);

                if (empresa.getDialaboralSabado().equals("1") && diaSemana == 7) {
                    cantDias++;
                } else if (empresa.getDialaboralDomingo().equals("1") && diaSemana == 1) {
                    cantDias++;
                } else if (empresa.getDialaboralFestivos().equals("1") && listadoDias.contains(fechaInicial)) {
                    cantDias++;
                } else {
                    if (cantDias == 0) {
                        break;
                    }
                }
                if (fechafinal) {
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                } else {
                    calendar.add(Calendar.DAY_OF_YEAR, -1);
                }

                fechaIniDate = calendar.getTime();
                fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

                cantDias--;
            }

            //logMsg.loggerMessageDebug("  |Fecha final  : " + fechaInicial);
        } catch (Exception e) {
            e.printStackTrace();
            //new LoggerMessage().loggerMessageError(e.getMessage());
        } finally {

        }
//       logMsg.loggerMessageDebug("5 >> " + fechaInicial);
        return fechaInicial;

    }

    public static int calcularDiasMes(int mes, int anyo) {

        int numDias = 0;

        switch (mes) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                numDias = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                numDias = 30;
                break;
            case 2:
                if ((anyo % 4 == 0 && anyo % 100 != 0) || anyo % 400 == 0) {
                    numDias = 29;
                } else {
                    numDias = 28;
                }
                break;
            default:
                // logMsg.loggerMessageDebug("  |\nEl mes " + mes + " es incorrecto.");
                break;
        }

        if (numDias != 0) {
            //  logMsg.loggerMessageDebug("  |\nEl mes " + mes + " del año " + anyo + " tiene " + numDias + " días.");
        }
        return numDias;

    }

    public static ArrayList<EjecucionTareaDTO> recurrenteDiarioSiguiente(EmpresaDTO empresa, int cantDias, TareaPredeterminadaDTO tarea, String fechaFinal) throws ParseException {
        ArrayList<EjecucionTareaDTO> listadoEjecucion = null;
        java.util.Date fechaIniDate = null;
        //ArrayList<String> listadoDias = null;
        EjecucionTareaDTO ejecucion = null;
        int diaSemana;
        boolean sumar = false;
        boolean bandera = false;
        try {

            //listadoDias = litarDiasFestivos();
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            String fechaInicial = "";
            System.out.println("  |fechafinal  " + fechaFinal);
            Calendar calendarFechaFinal = Calendar.getInstance();
            calendarFechaFinal.setTime(formato.parse(fechaFinal)); // Configuramos la fecha que se recibe

            if (calendarFechaFinal.after(calendar) || formato.format(calendarFechaFinal.getTime()).equals(formato.format(calendar.getTime()))) {
                calendarFechaFinal.add(Calendar.DAY_OF_YEAR, 1);
                fechaInicial = formato.format(calendarFechaFinal.getTime());
            } else {
                fechaInicial = formato.format(calendar.getTime());
            }

            //logMsg.loggerMessageDebug("  |Fecha hoy " + fechaInicial);
            //logMsg.loggerMessageDebug("  |Dia del mes " + calendar.get(Calendar.DAY_OF_MONTH));
            listadoEjecucion = new ArrayList<>();

            /**
             * Se ordenana las actividades segun el orden de actividad de manera
             * descendiente
             */
            //Collections.sort(tarea.getActividades(), (ActividadPredeterminadaDTO detalle1, ActividadPredeterminadaDTO detalle2) -> detalle1.getOrdenActividad().compareTo(detalle2.getOrdenActividad()));
            while (!bandera) {

                fechaIniDate = formato.parse(fechaInicial);
                calendar.setTime(fechaIniDate);
                diaSemana = calendar.get(Calendar.DAY_OF_WEEK);

                if (empresa.getDialaboralSabado().equals("1") && diaSemana == 7) {
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                    //cantDias--;

                } else if (empresa.getDialaboralDomingo().equals("1") && diaSemana == 1) {
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                    //cantDias--;("1"
                } else if (empresa.getDialaboralFestivos().equals("1") && listadoDias.contains(fechaInicial)) {
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                    // cantDias--;
                } else {
                    bandera = true;
                    fechaIniDate = calendar.getTime();
                    fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

                    ejecucion = new EjecucionTareaDTO();

                    ejecucion = llenarEjecucion(tarea, fechaInicial, empresa, listadoDias);
                    //ejecucion.setCodEjcucion("" + i);
                    listadoEjecucion.add(ejecucion);
                }

                calendar.add(Calendar.DAY_OF_YEAR, 1);

                //calendar.add(Calendar.DAY_OF_YEAR, 1);
                fechaIniDate = calendar.getTime();
                fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

            }
            tarea.setEjecuciones(listadoEjecucion);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listadoEjecucion;
    }

    public static void recurrenteMensualSiguientes(EmpresaDTO empresa, int diaMes, boolean diaHabilSiguiente, TareaPredeterminadaDTO tarea, Calendar fechaFinal) throws ParseException {

        ArrayList<EjecucionTareaDTO> listadoEjecucion = null;
        EjecucionTareaDTO ejecucion = null;
        java.util.Date fechaIniDate = null;
        //ArrayList<String> listadoDias = null;

        Calendar calendarHoy = null;
        Calendar calendar = null;
        Calendar calendar2 = null;

        String fechaInicial = Generales.EMPTYSTRING;

        SimpleDateFormat formato = null;
        boolean bandera = false;

        try {

            calendarHoy = Calendar.getInstance();

            calendar = Calendar.getInstance();
            calendar.set(Calendar.MONTH, fechaFinal.get(Calendar.MONTH));
            calendar.set(Calendar.DAY_OF_MONTH, diaMes);

            //listadoDias = litarDiasFestivos();
            formato = new SimpleDateFormat("yyyy-MM-dd");
            fechaInicial = formato.format(calendar.getTime());
//            logMsg.loggerMessageDebug("  |Fecha hoy " + fechaInicial);

            int diaSemana, diaSuamar;

            if (diaHabilSiguiente) {
                diaSuamar = 1;
            } else {
                diaSuamar = -1;
            }

            listadoEjecucion = new ArrayList<>();
            // Collections.sort(tarea.getActividades(), (ActividadPredeterminadaDTO detalle1, ActividadPredeterminadaDTO detalle2) -> detalle1.getOrdenActividad().compareTo(detalle2.getOrdenActividad()));

            while (!bandera) {
                if (calendar.after(calendarHoy) || formato.format(calendar.getTime()).equals(formato.format(calendarHoy.getTime()))) {
                    fechaIniDate = formato.parse(fechaInicial);
                    calendar.setTime(fechaIniDate);
                    //diaMesActual = calendar.get(Calendar.DAY_OF_MONTH);
                    diaSemana = calendar.get(Calendar.DAY_OF_WEEK);

                    calendar2 = Calendar.getInstance();
                    calendar2.setTime(calendar.getTime());
                    calendar2.add(Calendar.DAY_OF_YEAR, diaSuamar);
                    if (calendar2.get(Calendar.MONTH) != calendar.get(Calendar.MONTH)) {
                        diaSuamar = diaSuamar * -1;
                    }

                    if (empresa.getDialaboralSabado().equals("1") && diaSemana == 7) {

                        calendar.add(Calendar.DAY_OF_YEAR, diaSuamar);
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

                    } else if (empresa.getDialaboralDomingo().equals("1") && diaSemana == 1) {

                        calendar.add(Calendar.DAY_OF_YEAR, diaSuamar);
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

                    } else if (empresa.getDialaboralFestivos().equals("1") && listadoDias.contains(fechaInicial)) {

                        calendar.add(Calendar.DAY_OF_YEAR, diaSuamar);
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

                    } else {
                        bandera = true;
                        if (tarea.getActividades() != null) {
                            ejecucion = new EjecucionTareaDTO();
                            ejecucion = llenarEjecucion(tarea, fechaInicial, empresa, listadoDias);
                            //ejecucion.setCodEjcucion("" + i);
                            listadoEjecucion.add(ejecucion);
                        }
                        calendar.add(Calendar.MONTH, 1);
                        calendar.set(Calendar.DAY_OF_MONTH, diaMes);
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));
                    }

                } else {
                    calendar.add(Calendar.MONTH, 1);
                    calendar.set(Calendar.DAY_OF_MONTH, diaMes);
                    fechaIniDate = calendar.getTime();
                    fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));
                }

            }
            tarea.setFechaInicialReal(fechaInicial);
            tarea.setEjecuciones(listadoEjecucion);

//            logMsg.loggerMessageDebug("  |Tarea final == " + tarea.toStringJson());
        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public static ArrayList<EjecucionTareaDTO> recurrentesemanalSiguientes(EmpresaDTO empresa, int cantDias, int diaSemanatarea, TareaPredeterminadaDTO tarea, Calendar fechaFinal) throws ParseException {
        ArrayList<EjecucionTareaDTO> listadoEjecucion = null;
        java.util.Date fechaIniDate = null;
        //ArrayList<String> listadoDias = null;
        EjecucionTareaDTO ejecucion = null;
        int diaSemana;
        boolean bandera = false;
        try {

            //listadoDias = litarDiasFestivos();
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            String fechaInicial = formato.format(fechaFinal.getTime());
            System.out.println("  |Fecha hoy " + fechaInicial);
            System.out.println("  |Dia del mes " + calendar.get(Calendar.DAY_OF_MONTH));
            listadoEjecucion = new ArrayList<>();
            /**
             * Se ordenana las actividades segun el orden de actividad de manera
             * descendiente
             */
            //Collections.sort(tarea.getActividades(), (ActividadPredeterminadaDTO detalle1, ActividadPredeterminadaDTO detalle2) -> detalle1.getOrdenActividad().compareTo(detalle2.getOrdenActividad()));
            while (!bandera) {
                fechaIniDate = formato.parse(fechaInicial);
                calendar.setTime(fechaIniDate);
                diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
                if (diaSemana == diaSemanatarea) {
                    if (empresa.getDialaboralSabado().equals("1") && diaSemana == 7) {
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                        //cantDias--;

                    } else if (empresa.getDialaboralDomingo().equals("1") && diaSemana == 1) {
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                        //cantDias--;("1"
                    } else if (empresa.getDialaboralFestivos().equals("1") && listadoDias.contains(fechaInicial)) {
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                        // cantDias--;
                    } else {
                        bandera = true;
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

                        ejecucion = new EjecucionTareaDTO();

                        ejecucion = llenarEjecucion(tarea, fechaInicial, empresa, listadoDias);
                        //ejecucion.setCodEjcucion("" + i);
                        listadoEjecucion.add(ejecucion);
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                    }

                }
                calendar.add(Calendar.DAY_OF_YEAR, 1);

                fechaIniDate = calendar.getTime();
                fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

            }
            tarea.setEjecuciones(listadoEjecucion);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listadoEjecucion;
    }

    public static void recurrenteAnualSiguientes(EmpresaDTO empresa, int diaMes, int mes, boolean diaHabilSiguiente, TareaPredeterminadaDTO tarea, Calendar fechaFinal) throws ParseException {

        ArrayList<EjecucionTareaDTO> listadoEjecucion = null;
        //ArrayList<String> listadoDias = null;
        java.util.Date fechaIniDate = null;
        EjecucionTareaDTO ejecucion = null;
        Calendar calendarHoy = null;
        Calendar calendar = null;
        //Calendar calendar2 = null;
        SimpleDateFormat formato = null;
        String fechaInicial = Generales.EMPTYSTRING;
        int diaSemana, diaSuamar;

        try {
            //listadoDias = litarDiasFestivos();

            calendarHoy = Calendar.getInstance();
            calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH, diaMes);
            calendar.set(Calendar.MONTH, mes);
            calendar.set(Calendar.YEAR, fechaFinal.get(Calendar.YEAR));

            formato = new SimpleDateFormat("yyyy-MM-dd");
            fechaInicial = formato.format(calendar.getTime());
            //logMsg.loggerMessageDebug("  |Fecha de la recurrencia " + fechaInicial);

            if (diaHabilSiguiente) {
                diaSuamar = 1;
            } else {
                diaSuamar = -1;
            }

            listadoEjecucion = new ArrayList<>();

            if (calendar.before(calendarHoy)) {
                calendar.set(Calendar.YEAR, calendarHoy.get(Calendar.YEAR) + 1);
            }
            while (true) {
                if (calendarHoy.before(calendar) || calendarHoy.equals(calendar)) {

                    fechaIniDate = formato.parse(fechaInicial);
                    calendar.setTime(fechaIniDate);
                    //diaMesActual = calendar.get(Calendar.DAY_OF_MONTH);
                    diaSemana = calendar.get(Calendar.DAY_OF_WEEK);

                    //calendar2 = Calendar.getInstance();
                    calendarHoy.setTime(calendar.getTime());
                    calendarHoy.add(Calendar.DAY_OF_YEAR, diaSuamar);
                    if (calendarHoy.get(Calendar.MONTH) != calendar.get(Calendar.MONTH)) {
                        diaSuamar = diaSuamar * -1;
                    }

                    if (empresa.getDialaboralSabado().equals("1") && diaSemana == 7) {

                        calendar.add(Calendar.DAY_OF_YEAR, diaSuamar);
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

                    } else if (empresa.getDialaboralDomingo().equals("1") && diaSemana == 1) {

                        calendar.add(Calendar.DAY_OF_YEAR, diaSuamar);
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

                    } else if (empresa.getDialaboralFestivos().equals("1") && listadoDias.contains(fechaInicial)) {

                        calendar.add(Calendar.DAY_OF_YEAR, diaSuamar);
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

                    } else {
                        // calendar.add(Calendar.MONTH, 1);
                        // calendar.set(Calendar.DAY_OF_MONTH, diaMes);
                        // fechaIniDate = calendar.getTime();
                        //fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));
                        //  fechaValida=false;
                        ejecucion = new EjecucionTareaDTO();

                        ejecucion = llenarEjecucion(tarea, fechaInicial, empresa, listadoDias);
                        listadoEjecucion.add(ejecucion);

                        break;
                    }

                } else {
                    break;
                }
            }

            tarea.setEjecuciones(listadoEjecucion);

        } catch (Exception e) {
        }

    }

    public static ArrayList<EjecucionTareaDTO> recurrentesemanalSiguientesSabadoDomingo(EmpresaDTO empresa, int cantDias, int diaSemanatarea, TareaPredeterminadaDTO tarea, Calendar fechaFinal) throws ParseException {
        ArrayList<EjecucionTareaDTO> listadoEjecucion = null;
        java.util.Date fechaIniDate = null;
        EjecucionTareaDTO ejecucion = null;
        int diaSemana;
        boolean bandera = false;
        try {

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            String fechaInicial = formato.format(fechaFinal.getTime());
            listadoEjecucion = new ArrayList<>();

            while (!bandera) {
                fechaIniDate = formato.parse(fechaInicial);
                calendar.setTime(fechaIniDate);
                diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
                if (diaSemana == diaSemanatarea) {
                    if (empresa.getDialaboralFestivos().equals("1") && listadoDias.contains(fechaInicial)) {
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                        bandera = true;
                    } else {
                        bandera = true;
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);
                        ejecucion = new EjecucionTareaDTO();
                        ejecucion = llenarEjecucion(tarea, fechaInicial, empresa, listadoDias);
                        listadoEjecucion.add(ejecucion);
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                    }
                }
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                fechaIniDate = calendar.getTime();
                fechaInicial = formato.format(fechaIniDate);
            }

            tarea.setEjecuciones(listadoEjecucion);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listadoEjecucion;
    }

    public static String recurrenteDiarioGenerarProxima(EmpresaDTO empresa, int cantDias, TareaPredeterminadaDTO tarea, String fechaFinal) throws ParseException {
        ArrayList<EjecucionTareaDTO> listadoEjecucion = null;
        java.util.Date fechaIniDate = null;
        //ArrayList<String> listadoDias = null;
        int diaSemana;
        boolean bandera = false;
        String fechaInicial = "";

        try {

            //listadoDias = litarDiasFestivos();
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

            Calendar calendarFechaFinal = Calendar.getInstance();
            calendarFechaFinal.setTime(formato.parse(fechaFinal)); // Configuramos la fecha que se recibe

            if (calendarFechaFinal.after(calendar) || formato.format(calendarFechaFinal.getTime()).equals(formato.format(calendar.getTime()))) {
                calendarFechaFinal.add(Calendar.DAY_OF_YEAR, 1);
                fechaInicial = formato.format(calendarFechaFinal.getTime());
            } else {
                fechaInicial = formato.format(calendar.getTime());
            }

            //logMsg.loggerMessageDebug("  |Fecha hoy " + fechaInicial);
            //logMsg.loggerMessageDebug("  |Dia del mes " + calendar.get(Calendar.DAY_OF_MONTH));
            listadoEjecucion = new ArrayList<>();

            /**
             * Se ordenana las actividades segun el orden de actividad de manera
             * descendiente
             */
            //Collections.sort(tarea.getActividades(), (ActividadPredeterminadaDTO detalle1, ActividadPredeterminadaDTO detalle2) -> detalle1.getOrdenActividad().compareTo(detalle2.getOrdenActividad()));
            while (!bandera) {

                fechaIniDate = formato.parse(fechaInicial);
                calendar.setTime(fechaIniDate);
                diaSemana = calendar.get(Calendar.DAY_OF_WEEK);

                if (empresa.getDialaboralSabado().equals("1") && diaSemana == 7) {
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                    //cantDias--;

                } else if (empresa.getDialaboralDomingo().equals("1") && diaSemana == 1) {
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                    //cantDias--;("1"
                } else if (empresa.getDialaboralFestivos().equals("1") && listadoDias.contains(fechaInicial)) {
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                    // cantDias--;
                } else {
                    bandera = true;
                    fechaIniDate = calendar.getTime();
                    fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));
                }
                fechaIniDate = calendar.getTime();
                fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

            }
            tarea.setEjecuciones(listadoEjecucion);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return fechaInicial;
    }

    public static void recurrenteMensualGenerarProximas(EmpresaDTO empresa, int diaMes, boolean diaHabilSiguiente, TareaPredeterminadaDTO tarea, Calendar fechaFinal) throws ParseException {
        java.util.Date fechaIniDate = null;
        //ArrayList<String> listadoDias = null;

        Calendar calendarHoy = null;
        Calendar calendar = null;
        Calendar calendar2 = null;

        String fechaInicial = Generales.EMPTYSTRING;

        SimpleDateFormat formato = null;
        boolean bandera = false;

        try {

            calendarHoy = Calendar.getInstance();

            calendar = Calendar.getInstance();
            calendar.set(Calendar.MONTH, fechaFinal.get(Calendar.MONTH));

            if (calendar.get(Calendar.DAY_OF_MONTH) > diaMes || tarea.getNueva().equals("1")) {
                calendar.add(Calendar.MONTH, 1);
            }
            calendar.set(Calendar.DAY_OF_MONTH, diaMes);

            //listadoDias = litarDiasFestivos();
            formato = new SimpleDateFormat("yyyy-MM-dd");
            fechaInicial = formato.format(calendar.getTime());

            int diaSemana, diaSuamar;

            if (diaHabilSiguiente) {
                diaSuamar = 1;
            } else {
                diaSuamar = -1;
            }

            // Collections.sort(tarea.getActividades(), (ActividadPredeterminadaDTO detalle1, ActividadPredeterminadaDTO detalle2) -> detalle1.getOrdenActividad().compareTo(detalle2.getOrdenActividad()));
            while (!bandera) {
                if (calendar.after(calendarHoy) || formato.format(calendar.getTime()).equals(formato.format(calendarHoy.getTime()))) {
                    fechaIniDate = formato.parse(fechaInicial);
                    calendar.setTime(fechaIniDate);
                    //diaMesActual = calendar.get(Calendar.DAY_OF_MONTH);
                    diaSemana = calendar.get(Calendar.DAY_OF_WEEK);

                    calendar2 = Calendar.getInstance();
                    calendar2.setTime(calendar.getTime());
                    calendar2.add(Calendar.DAY_OF_YEAR, diaSuamar);
                    if (calendar2.get(Calendar.MONTH) != calendar.get(Calendar.MONTH)) {
                        diaSuamar = diaSuamar * -1;
                    }

                    if (empresa.getDialaboralSabado().equals("1") && diaSemana == 7) {

                        calendar.add(Calendar.DAY_OF_YEAR, diaSuamar);
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

                    } else if (empresa.getDialaboralDomingo().equals("1") && diaSemana == 1) {

                        calendar.add(Calendar.DAY_OF_YEAR, diaSuamar);
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

                    } else if (empresa.getDialaboralFestivos().equals("1") && listadoDias.contains(fechaInicial)) {
                        calendar.add(Calendar.DAY_OF_YEAR, diaSuamar);
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

                    } else {
                        bandera = true;
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));
                        tarea.setFechaInicialReal(fechaInicial);

                    }

                } else {
                    bandera = true;
                    fechaIniDate = calendar.getTime();
                    fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));
                    tarea.setFechaInicialReal(fechaInicial);
                }

            }

//            logMsg.loggerMessageDebug("  |Tarea final == " + tarea.toStringJson());
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    public static String recurrentesemanalGenerarProximas(EmpresaDTO empresa, int cantDias, int diaSemanatarea, TareaPredeterminadaDTO tarea, Calendar fechaFinal, boolean diaHabilSiguiente) throws ParseException {
        java.util.Date fechaIniDate = null;
        int diaSemana;
        boolean bandera = false, habil = false;
        int sumar = 1;
        String fechaInicial = Generales.EMPTYSTRING;
        try {

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            fechaFinal.add(Calendar.DAY_OF_YEAR, 1);
            fechaInicial = formato.format(fechaFinal.getTime());

            /**
             * Se ordenana las actividades segun el orden de actividad de manera
             * descendiente
             */
            //Collections.sort(tarea.getActividades(), (ActividadPredeterminadaDTO detalle1, ActividadPredeterminadaDTO detalle2) -> detalle1.getOrdenActividad().compareTo(detalle2.getOrdenActividad()));
            while (!bandera) {
                fechaIniDate = formato.parse(fechaInicial);
                calendar.setTime(fechaIniDate);
                diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
                if (diaSemana == diaSemanatarea) {
                    if (empresa.getDialaboralFestivos().equals("1") && listadoDias.contains(fechaInicial)) {
                        bandera = true;
                        if (!diaHabilSiguiente) {
                            sumar = -1;
                        }
                        while (!habil) {
                            if (empresa.getDialaboralSabado().equals("1") && diaSemana == 7) {
                                calendar.add(Calendar.DAY_OF_YEAR, sumar);
                            } else if (empresa.getDialaboralDomingo().equals("1") && diaSemana == 1) {
                                calendar.add(Calendar.DAY_OF_YEAR, sumar);
                            } else if (empresa.getDialaboralFestivos().equals("1") && listadoDias.contains(fechaInicial)) {
                                calendar.add(Calendar.DAY_OF_YEAR, sumar);
                            } else {
                                habil = true;
                                fechaIniDate = calendar.getTime();
                                fechaInicial = formato.format(fechaIniDate);

                            }
                            fechaIniDate = calendar.getTime();
                            fechaInicial = formato.format(fechaIniDate);
                            diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
                        }
                    } else {
                        bandera = true;
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));
                    }

                } else {
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                }

                fechaIniDate = calendar.getTime();
                fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

            }

            tarea.setFechaInicialReal(fechaInicial);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return fechaInicial;
    }

    public static void recurrenteAnualGenerarProximas(EmpresaDTO empresa, int diaMes, int mes, boolean diaHabilSiguiente, TareaPredeterminadaDTO tarea, Calendar fechaFinal) throws ParseException {

        ArrayList<EjecucionTareaDTO> listadoEjecucion = null;
        //ArrayList<String> listadoDias = null;
        java.util.Date fechaIniDate = null;
        EjecucionTareaDTO ejecucion = null;
        Calendar calendarHoy = null;
        Calendar calendar = null;
        //Calendar calendar2 = null;
        SimpleDateFormat formato = null;
        String fechaInicial = Generales.EMPTYSTRING;
        int diaSemana, diaSuamar;

        try {
            //listadoDias = litarDiasFestivos();

            calendarHoy = Calendar.getInstance();
            calendar = Calendar.getInstance();

//para saber si hay ejecuciones recien registradas, para evitar duplicar la fecha
            if (tarea.getNueva().equals("1")) {
                calendar.set(Calendar.YEAR, fechaFinal.get(Calendar.YEAR) + 1);
            } else {
                calendar.set(Calendar.YEAR, fechaFinal.get(Calendar.YEAR));
            }

            calendar.set(Calendar.DAY_OF_MONTH, diaMes);
            calendar.set(Calendar.MONTH, mes);

            formato = new SimpleDateFormat("yyyy-MM-dd");
            //logMsg.loggerMessageDebug("  |Fecha de la recurrencia " + fechaInicial);

            if (diaHabilSiguiente) {
                diaSuamar = 1;
            } else {
                diaSuamar = -1;
            }
            //           logMsg.loggerMessageDebug("dias sumar **************  " + diaSuamar);

            listadoEjecucion = new ArrayList<>();
//           logMsg.loggerMessageDebug("antes del if anual ->" + formato.format(calendar.getTime()) + " ><><> " + formato.format(calendarHoy.getTime()));
            if (calendar.before(calendarHoy)) {
                calendar.set(Calendar.YEAR, calendarHoy.get(Calendar.YEAR) + 1);
//               logMsg.loggerMessageDebug("estra if calendar--" + formato.format(calendar.getTime()));
            }
            fechaInicial = formato.format(calendar.getTime());

            while (true) {
                if (calendarHoy.before(calendar) || calendarHoy.equals(calendar)) {

                    fechaIniDate = formato.parse(fechaInicial);
                    calendar.setTime(fechaIniDate);
                    //diaMesActual = calendar.get(Calendar.DAY_OF_MONTH);
                    diaSemana = calendar.get(Calendar.DAY_OF_WEEK);

                    //calendar2 = Calendar.getInstance();
                    calendarHoy.setTime(calendar.getTime());
                    calendarHoy.add(Calendar.DAY_OF_YEAR, diaSuamar);
                    if (calendarHoy.get(Calendar.MONTH) != calendar.get(Calendar.MONTH)) {
                        diaSuamar = diaSuamar * -1;
                    }

                    if (empresa.getDialaboralSabado().equals("1") && diaSemana == 7) {

                        calendar.add(Calendar.DAY_OF_YEAR, diaSuamar);
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

                    } else if (empresa.getDialaboralDomingo().equals("1") && diaSemana == 1) {

                        calendar.add(Calendar.DAY_OF_YEAR, diaSuamar);
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

                    } else if (empresa.getDialaboralFestivos().equals("1") && listadoDias.contains(fechaInicial)) {

                        calendar.add(Calendar.DAY_OF_YEAR, diaSuamar);
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);//Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));

                    } else {
                        // calendar.add(Calendar.MONTH, 1);
                        // calendar.set(Calendar.DAY_OF_MONTH, diaMes);
                        // fechaIniDate = calendar.getTime();
                        //fechaInicial = Formato.formatoFecha(DateFormat.getDateInstance().format(fechaIniDate));
                        //  fechaValida=false;
                        ejecucion = new EjecucionTareaDTO();

                        //ejecucion = llenarEjecucion(tarea, fechaInicial, empresa, listadoDias);
                        listadoEjecucion.add(ejecucion);

                        break;
                    }

                } else {
                    break;
                }
            }
            tarea.setFechaInicialReal(fechaInicial);
            tarea.setEjecuciones(listadoEjecucion);

        } catch (Exception e) {
        }

    }

    public static String recurrentesemanalSabadoDomingoGenerarProximas(EmpresaDTO empresa, int cantDias, int diaSemanatarea, TareaPredeterminadaDTO tarea, Calendar fechaFinal) throws ParseException {
        java.util.Date fechaIniDate = null;
        int diaSemana;
        boolean bandera = false;
        String fechaInicial = Generales.EMPTYSTRING;
        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            fechaFinal.add(Calendar.DAY_OF_YEAR, 1);
            fechaInicial = formato.format(fechaFinal.getTime());

            while (!bandera) {
                fechaIniDate = formato.parse(fechaInicial);
                calendar.setTime(fechaIniDate);
                diaSemana = calendar.get(Calendar.DAY_OF_WEEK);

//                logMsg.loggerMessageDebug(" | dia semanaProxima   " + diaSemana + "  | dia tareaProxima   " + diaSemanatarea + "  -> y-> " + y + " ----->> " + calendar.getTime());
                if (diaSemana == diaSemanatarea) {
                    if (empresa.getDialaboralFestivos().equals("1") && listadoDias.contains(fechaInicial)) {
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                        System.out.println("primer caso ");
                        bandera = true;
                    } else {
                        System.out.println("segundo caso ");
                        bandera = true;
                        fechaIniDate = calendar.getTime();
                        fechaInicial = formato.format(fechaIniDate);
//                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                    }
                } else {
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                }
                fechaIniDate = calendar.getTime();
                fechaInicial = formato.format(fechaIniDate);
            }

            tarea.setFechaInicialReal(fechaInicial);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return fechaInicial;
    }

}
